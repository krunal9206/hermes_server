﻿const { Op } = require("sequelize");
const InterfaceCard = require("../models").InterfaceCard;
const User = require("../models").User;
const Stakeholder = require("../models").Stakeholder;
const Asset = require("../models").Asset;
const mAssetType = require("../models").mAssetType;
const tDeviceReportedErrWrn = require("../models").tDeviceReportedErrWrn;
const mDeviceInstance = require("../models").mDeviceInstance;
const mDevice = require("../models").mDevice;

const getPlantInfo = async (params) => {
  const { assestIds } = params;

  const currentAssestId = assestIds[0];

  const currentAsset = await Asset.findByPk(currentAssestId);

  const currentStakeholder = await Stakeholder.findByPk(
    currentAsset.stakeholderId
  );

  const interfaceCards = [];

  if (currentAsset) {
    const currentAssetType = await mAssetType.findByPk(
      currentAsset.systemAssetTypeId
    );

    const currentDeviceInstance = await mDeviceInstance.findOne({
      where: {
        systemAssetId: currentAsset.id,
        status: "enable",
      },
    });

    const currentDevice = await mDevice.findByPk(
      currentDeviceInstance.systemDeviceId
    );

    interfaceCards.push({
      id: currentAsset.id,
      cardName: currentAsset.assetName,
      cardAttr: currentAsset.addlAttributesSetupByAdmin,
      cardTypeAttr: currentAssetType.addlAttributesCollectionTemplate,
      deviceAttr: currentDeviceInstance.addlAttributesSetupByAdmin,
      deviceTypeAttr: currentDevice.addlAttributesCollectionTemplate,
      stakeholder: currentStakeholder,
    });
  }

  return interfaceCards;

  //   const interfaceCardsResults = await InterfaceCard.findAll({
  //     where: {
  //       status: "enable",
  //       assetId: currentAssestId,
  //     },
  //   });

  //   const interfaceCards = [];

  //   for (const interfaceCard of interfaceCardsResults) {
  //     const interfacecardType = await InterfacecardType.findByPk(
  //       interfaceCard.interfaceCardTypeId
  //     );
  //     interfaceCards.push({
  //       id: interfaceCard.id,
  //       cardName: interfaceCard.cardName,
  //       serialNo: interfaceCard.serialNo,
  //       cardAttr: interfaceCard.addAttributes,
  //       cardTypeAttr: interfacecardType.addAttributes,
  //     });
  //   }

  //   return interfaceCards;
};

const getAlerts = async (params, currentUserId) => {
  let response = {};

  const currentUser = await User.findByPk(currentUserId);
  const currentStakeholder = await Stakeholder.findByPk(
    currentUser.stakeholderId
  );

  if (currentStakeholder) {
    let whereObject = {};
    whereObject.givenStakeholderId = {
      [Op.eq]: currentStakeholder.givenStakeholderId,
    };

    let sortOrder = [["systemErrWrnTxnId", "desc"]];

    const { page, _limit } = params;

    const results = await tDeviceReportedErrWrn.findAndCountAll({
      where: whereObject,
      include: ["DeviceInstance", "DeviceErrWrnCode"],
      offset: (page - 1) * _limit,
      limit: _limit,
      order: sortOrder,
    });

    const { count, rows } = results;

    let data = [];
    for (const row of rows) {
      // console.log(row);
      const generatedDateTime = new Date(row.generatedDateTime * 1000);
      data.push({
        systemErrWrnTxnId: row.systemErrWrnTxnId,
        generatedDateTime: generatedDateTime.toLocaleString(),
        serialNo: row.DeviceInstance.serialNo,
        givenDeviceErrWrnCode: row.givenDeviceErrWrnCode,
        errWrnCodeType: row.DeviceErrWrnCode.errWrnCodeType,
        errWrnDescription: row.DeviceErrWrnCode.errWrnDescription,
      });
    }

    response["data"] = data;
    response["total"] = count;
  } else {
    response["data"] = [];
  }
  return response;
};

const getDeviceInfo = async (params) => {
  const { assestIds } = params;
  const currentAssestId = assestIds[0];
  const currentAsset = await Asset.findByPk(currentAssestId);

  let response = {};

  if (currentAsset) {
    const mDeviceInstances = await mDeviceInstance.findAll({
      where: {
        systemAssetId: currentAsset.id,
        status: "enable",
      },
      raw: true,
      nest: true,
    });

    let systemDeviceIds = [];

    let deviceObj = {};

    for (const device of mDeviceInstances) {
      systemDeviceIds.push(device.systemDeviceId);
      if (Array.isArray(deviceObj[device.systemDeviceId])) {
        deviceObj[device.systemDeviceId].push(device);
      } else {
        deviceObj[device.systemDeviceId] = [device];
      }
    }

    let uniqSystemDeviceIds = [...new Set(systemDeviceIds)];

    const deviceTypes = await mDevice.findAll({
      where: {
        systemDeviceId: uniqSystemDeviceIds,
        status: "enable",
      },
      include: ["devicetype"],
      raw: true,
      nest: true,
    });

    response.id = currentAsset.id;
    response.devices = deviceObj;
    response.devicetypes = deviceTypes;
  }

  return response;
};

module.exports = {
  getPlantInfo,
  getAlerts,
  getDeviceInfo,
};
