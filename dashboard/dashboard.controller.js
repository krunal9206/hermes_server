﻿const express = require("express");
const router = express.Router();
const service = require("./dashboard.service");

// routes
router.post("/plantinfo", getPlantInfo);
router.get("/alerts", getAlerts);
router.post("/deviceinfo", getDeviceInfo);

module.exports = router;

function getPlantInfo(req, res, next) {
  service
    .getPlantInfo(req.body)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getDeviceInfo(req, res, next) {
  service
    .getDeviceInfo(req.body)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getAlerts(req, res, next) {
  service
    .getAlerts(req.query, req.user.user_id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}
