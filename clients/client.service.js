﻿const { Op } = require("sequelize");
const Modal = require('../models').Client;

const getData = async (params) => {
    const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

    let whereObject = {};
    whereObject.status = {
        [Op.ne]: 'deleted'
    };

    if(searchTerm) {
        whereObject.clientName = {
            [Op.iLike]: `%${searchTerm}%`
        }
    }

    let sortOrder = [];
    if(sortColumn && sortDirection) {
        sortOrder = [[sortColumn, sortDirection]]
    } else {
        sortOrder = [['id', 'desc']]
    }

    return { count, rows } = await Modal.findAndCountAll({
        where: whereObject,
        offset: (page - 1) * pageSize,
        limit: pageSize,
        order: sortOrder
    });
};

const createData = async (data, currentUserId) => {
    data.createdBy = data.updateBy = currentUserId;
    const date = `${data.activationDate.year}-${('0' + data.activationDate.month).slice(-2)}-${('0' + data.activationDate.day).slice(-2)}`;
    const time = `${('0' + data.activationTime.hour).slice(-2)}:${('0' + data.activationTime.minute).slice(-2)}:00`;
    const activationDateTime = `${date} ${time} GMT+5:30`;
    data.activationDateTime = activationDateTime;

    const modal = await Modal.create(data);
    return modal;
}

const getDataById = async (id) => {
    const result = await Modal.findByPk(id);
    return result;
};

const updateData = async (data, currentUserId) => {
    data.updateBy = currentUserId;
    const date = `${data.activationDate.year}-${('0' + data.activationDate.month).slice(-2)}-${('0' + data.activationDate.day).slice(-2)}`;
    const time = `${('0' + data.activationTime.hour).slice(-2)}:${('0' + data.activationTime.minute).slice(-2)}:00`;
    const activationDateTime = `${date} ${time} GMT+5:30`;
    data.activationDateTime = activationDateTime;
    await Modal.update(data, {
        where: {
          id: data.id
        }
    });
    return true;
}

const deleteData = async (id) => {
    await Modal.update({'status': 'deleted'}, {
        where: {
          id: id
        }
    });
    return true;
}

module.exports = {
    getData,
    createData,
    getDataById,
    updateData,
    deleteData,
};