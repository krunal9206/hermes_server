﻿const express = require('express');
const router = express.Router();
const service = require('./client.service');
const Client = require('../models').Client;
const { check, validationResult } = require("express-validator");
const { Op } = require("sequelize");

// routes
router.post('/', [
    check('givenClientId', 'Client Id cannot be left blank').trim().custom(value => {
        return new Promise((resolve, reject) => {
            Client.findOne({ where: { givenClientId: value } }).then(client => {
                if(client) return reject(new Error('This client id is already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
], createData);
router.get('/', getData);
router.get('/:id', getDataById);
router.put('/:id', [
    check('givenClientId', 'Client Id cannot be left blank').trim().trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            Client.findOne({
                where: { 
                    givenClientId: value,
                    id: {
                        [Op.ne]: req.body.id
                    }
                } 
            }).then(client => {
                if(client) return reject(new Error('This client id is already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
], updateData);
router.delete('/:id', deleteData);
router.get('/get/contexts', getContexts);

module.exports = router;

function createData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }

    service.createData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getData(req, res, next) {
    service.getData(req.query)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDataById(req, res, next) {
    service.getDataById(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function updateData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }

    service.updateData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function deleteData(req, res, next) {
    service.deleteData(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getContexts(req, res, next) {
    service.getContexts()
        .then(result => res.json(result))
        .catch(err => next(err));
}