﻿const express = require('express');
const router = express.Router();
const service = require('./stakeholder.service');
const Stakeholder = require('../models').Stakeholder;
const { check, validationResult } = require("express-validator");
const { Op } = require("sequelize");

// routes
router.post('/', [
    check('givenStakeholderId', 'Stakeholder Id cannot be left blank').trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            Stakeholder.findOne({ 
                where: { 
                    givenStakeholderId: value,
                } 
            }).then(result => {
                if(result) return reject(new Error('This Stakeholder Id is already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
], createData);
router.get('/', getEntities);
router.get('/:id', getDataById);
router.put('/:id', [
    check('givenStakeholderId', 'Stakeholder Id cannot be left blank').trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            Stakeholder.findOne({ 
                where: { 
                    givenStakeholderId: value,
                    id: {
                        [Op.ne]: req.body.id
                    }
                } 
            }).then(result => {
                if(result) return reject(new Error('This Stakeholder Id is already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
], updateData);
router.delete('/:id', deleteData);
router.get('/get/stakeholdertypes', getStakeholdertypes);

module.exports = router;

function createData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }

    service.createData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getEntities(req, res, next) {
    service.getEntities(req.query)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDataById(req, res, next) {
    service.getDataById(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function updateData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }
    
    service.updateData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function deleteData(req, res, next) {
    service.deleteData(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getStakeholdertypes(req, res, next) {
    service.getStakeholdertypes()
        .then(result => res.json(result))
        .catch(err => next(err));
}