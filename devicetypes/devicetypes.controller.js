﻿const express = require('express');
const router = express.Router();
const service = require('./devicetype.service');
const DeviceType = require('../models').DeviceType;
const { check, validationResult } = require("express-validator");
const { Op } = require("sequelize");

// routes
router.post('/', [
    check('givenDeviceTypeId', 'Device Type Id cannot be left blank').trim().custom((value) => {
        return new Promise((resolve, reject) => {
            DeviceType.findOne({ 
                where: { 
                    givenDeviceTypeId: value,
                } 
            }).then(result => {
                if(result) return reject(new Error('This Device Type Id is already exists.'))
                else return resolve(value)
            }).catch(err => {
                return reject(err)
            });
        })
    }),
], createData);
router.get('/', getData);
router.get('/:id', getDataById);
router.put('/:id', [
    check('givenDeviceTypeId', 'Device Type Id cannot be left blank').trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            DeviceType.findOne({ 
                where: { 
                    givenDeviceTypeId: value,
                    id: {
                        [Op.ne]: req.body.id
                    }
                } 
            }).then(result => {
                if(result) return reject(new Error('This Device Type Id is already exists.'))
                else return resolve(value)
            }).catch(err => {
                return reject(err)
            });
        })
    }),
], updateData);
router.delete('/:id', deleteData);

module.exports = router;

function createData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }

    service.createData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getData(req, res, next) {
    service.getData(req.query)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDataById(req, res, next) {
    service.getDataById(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function updateData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }
    
    service.updateData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function deleteData(req, res, next) {
    service.deleteData(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}