'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('tDeviceReportedErrWrns', {
      systemErrWrnTxnId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      givenStakeholderId: {
        type: Sequelize.STRING
      },
      givenParentDeviceInstanceId: {
        type: Sequelize.STRING
      },
      givenDeviceInstanceId: {
        type: Sequelize.STRING
      },
      givenDeviceErrWrnCode: {
        type: Sequelize.STRING
      },
      generatedDateTime: {
        type: Sequelize.BIGINT,
      },
      resolvedDateTime: {
        type: Sequelize.BIGINT,
      },
      createdBy: {
        type: Sequelize.INTEGER,
      },
      updatedBy: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tDeviceReportedErrWrns');
  }
};