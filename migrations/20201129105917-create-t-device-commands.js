"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("tDeviceCommands", {
      serverTxnId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT,
      },
      givenStakeholderId: {
        type: Sequelize.STRING,
      },
      givenDeviceInstanceId: {
        type: Sequelize.STRING,
      },
      commandId: {
        type: Sequelize.STRING,
      },
      clientTxnId: {
        type: Sequelize.STRING,
      },
      clientTxnSentDateTime: {
        type: Sequelize.BIGINT,
      },
      clientTxnCaptureTime: {
        type: Sequelize.BIGINT,
      },
      responseCode: {
        type: Sequelize.INTEGER,
      },
      measurementJSONSet: {
        type: Sequelize.JSONB,
      },
      status: {
        type: Sequelize.STRING,
        defaultValue: "active",
      },
      createdBy: {
        type: Sequelize.INTEGER,
      },
      updatedBy: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("tDeviceCommands");
  },
};
