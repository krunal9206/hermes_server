"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("mDeviceErrWrnCodes", {
      systemDeviceErrWrnId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      givenDeviceErrWrnCode: {
        type: Sequelize.STRING,
      },
      systemDeviceId: {
        type: Sequelize.INTEGER,
      },
      errWrnCodeType: {
        type: Sequelize.ENUM("E", "W"),
      },
      errWrnDescription: {
        type: Sequelize.TEXT,
      },
      status: {
        type: Sequelize.STRING,
        defaultValue: "enable",
      },
      createdBy: {
        type: Sequelize.INTEGER,
      },
      updatedBy: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("mDeviceErrWrnCodes");
  },
};
