"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("tDeviceTelemetries", {
      serverTxnId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT,
      },
      givenStakeholderId: {
        type: Sequelize.STRING,
      },
      givenDeviceInstanceId: {
        type: Sequelize.STRING,
      },
      clientTxnId: {
        type: Sequelize.STRING,
      },
      clientTxnSentDateTime: {
        type: Sequelize.BIGINT,
      },
      clientTxnCaptureTime: {
        type: Sequelize.BIGINT,
      },
      clientSoftwareVersion: {
        type: Sequelize.STRING,
      },
      clientDataFormatVersion: {
        type: Sequelize.STRING,
      },
      measurementJSONSet: {
        type: Sequelize.JSONB,
      },
      status: {
        type: Sequelize.STRING,
        defaultValue: "active",
      },
      createdBy: {
        type: Sequelize.INTEGER,
      },
      updatedBy: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("tDeviceTelemetries");
  },
};
