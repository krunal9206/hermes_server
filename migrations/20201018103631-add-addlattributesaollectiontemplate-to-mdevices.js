"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("mDevices", "givenDeviceId", {
          transaction: t,
        }),
        queryInterface.addColumn(
          "mDevices",
          "addlAttributesCollectionTemplate",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "mDevices",
          "givenDeviceId",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.removeColumn(
          "mDevices",
          "addlAttributesCollectionTemplate",
          {
            transaction: t,
          }
        ),
      ]);
    });
  },
};
