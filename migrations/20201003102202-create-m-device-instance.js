"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("mDeviceInstances", {
      systemDeviceInstanceId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      givenDeviceInstanceId: {
        type: Sequelize.STRING,
      },
      systemDeviceId: {
        type: Sequelize.INTEGER,
      },
      systemAssetId: {
        type: Sequelize.INTEGER,
      },
      parentDeviceInstanceId: {
        type: Sequelize.INTEGER,
      },
      serialNo: {
        type: Sequelize.STRING,
      },
      productionDate: {
        type: Sequelize.DATEONLY,
      },
      goLiveDate: {
        type: Sequelize.DATEONLY,
      },
      addlAttributesSetupByAdmin: {
        type: Sequelize.TEXT,
      },
      addlAttributesSentByDevice: {
        type: Sequelize.TEXT,
      },
      latitude: {
        type: Sequelize.STRING,
      },
      longitude: {
        type: Sequelize.STRING,
      },
      status: {
        type: Sequelize.STRING,
        defaultValue: "enable",
      },
      createdBy: {
        type: Sequelize.INTEGER,
      },
      updatedBy: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("mDeviceInstances");
  },
};
