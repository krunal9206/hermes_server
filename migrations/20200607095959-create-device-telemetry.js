'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DeviceTelemetries', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.BIGINT
      },
      givenClientId: {
        type: Sequelize.STRING
      },
      givenInterfaceCardId: {
        type: Sequelize.STRING
      },
      givenDeviceId: {
        type: Sequelize.STRING
      },
      clientTxnDateTime: {
        type: Sequelize.STRING
      },
      shortMeasurementName: {
        type: Sequelize.STRING
      },
      deviceValue: {
        type: Sequelize.STRING
      },
      deviceValueUnit: {
        type: Sequelize.STRING
      },
      deviceTime: {
        type: Sequelize.STRING
      },
      addlJson: {
        type: Sequelize.TEXT
      },
      status: {
        type: Sequelize.STRING,
        defaultValue: 'enable'
      },
      createdBy: {
        type: Sequelize.STRING
      },
      updatedBy: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('DeviceTelemetries');
  }
};