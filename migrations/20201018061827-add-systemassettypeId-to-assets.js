"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "Assets",
          "systemAssetTypeId",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "Assets",
          "addlAttributesSetupByAdmin",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
      ]);
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("Assets", "systemAssetTypeId", {
          transaction: t,
        }),
        queryInterface.removeColumn("Assets", "addlAttributesSetupByAdmin", {
          transaction: t,
        }),
      ]);
    });
  },
};
