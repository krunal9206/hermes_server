﻿const express = require('express');
const router = express.Router();
const service = require('./stakeholdertype.service');
const StakeholderType = require('../models').mStakeholderType;
const { check, validationResult } = require("express-validator");
const { Op } = require("sequelize");

const myLogger = function (req, res, next) {
    console.info(`Parameters : ${JSON.stringify(req.params)}`);
    //console.info(`Header : ${req.header}`);
    let output = '';
    for (property in req.body) {
        output += property + ' : ' + req.body[property] + ',';
    }
    console.info(`Body : ${output}`);
    next();
}

// routes
router.use(myLogger);
router.post('/', [
    check('stakeholderName', 'Stakeholder Type Id cannot be left blank').trim().custom((value) => {
        return new Promise((resolve, reject) => {
            StakeholderType.findOne({ 
                where: { 
                    stakeholderName: value,
                } 
            }).then(result => {
                if(result) return reject(new Error('This Stakeholder Type Id is already exists.'))
                else return resolve(value)
            }).catch(err => {
                return reject(err)
            });
        })
    }),
], createData);
router.get('/', getData);
router.get('/:stakeholderId', getDataById);
router.put('/:stakeholderId', [
    check('stakeholderName', 'Stakeholder Type Id cannot be left blank').trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            StakeholderType.findOne({ 
                where: { 
                    stakeholderName: value,
                    /*id: {
                        [Op.ne]: req.body.id
                    }*/
                } 
            }).then(result => {
                if(result) return reject(new Error('This Stakeholder Type Id is already exists.'))
                else return resolve(value)
            }).catch(err => {
                return reject(err)
            });
        })
    }),
], updateData);
router.delete('/:stakeholderId', deleteData);

module.exports = router;

function createData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }

    service.createData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getData(req, res, next) {
    service.getData(req.query)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDataById(req, res, next) {
    service.getDataById(req.params.stakeholderId)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function updateData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }
    
    service.updateData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function deleteData(req, res, next) {
    service.deleteData(req.params.stakeholderId)
        .then(result => res.json(result))
        .catch(err => next(err));
}