﻿const { Op } = require("sequelize");
const deviceMeasurement = require('../../hermespersistence/models').deviceMeasurement;
const Device = require('../models').DeviceType;
const Measurement = require('../models').Measurement;

const getDeviceMeasurements = async (params) => {
    const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

    let whereObject = {};
    /* whereObject.status = {
        [Op.ne]: 'deleted'
    }; */

    if(searchTerm) {
        whereObject.deviceName = {
            [Op.iLike]: `%${searchTerm}%`
        }
    }

    let sortOrder = [];
    if(sortColumn && sortDirection) {
        sortOrder = [[sortColumn, sortDirection]]
    } else {
        sortOrder = [['id', 'desc']]
    }

    return { count, rows } = await deviceMeasurement.findAndCountAll({
        where: whereObject,
        // include: ['interfacecard'],
        offset: (page - 1) * pageSize,
        limit: pageSize,
        order: sortOrder
    });
};

const createData = async (data, currentUserId) => {
    data.createdBy = data.updateBy = currentUserId;

    const modal = await deviceMeasurement.create(data);
    return modal;
}

const getDeviceMeasurementsById = async (id) => {
    const idSet = id.split(',');
    return await deviceMeasurement.findAll({
        where: {
            id: {
                [Op.in]: idSet
            }
        }
    });
};

const updateData = async (data, currentUserId) => {
    data.updateBy = currentUserId;

    await deviceMeasurement.update(data, {
        where: {
          id: data.id
        }
    });
    return true;
}

const deleteData = async (id) => {
    await deviceMeasurement.update({'status': 'deleted'}, {
        where: {
          id: id
        }
    });
    return true;
}

const getDevices = async () => {
    const results = await Device.findAll({
        where: {
            status: 'enable'
        },
    });

    return results;
};

const getMeasurements = async () => {
    const results = await Measurement.findAll({
        where: {
            status: 'enable'
        },
    });

    return results;
};

module.exports = {
    getDeviceMeasurements,
    createData,
    getDeviceMeasurementsById,
    updateData,
    deleteData,
    getDevices,
    getMeasurements
};