﻿const express = require('express');
const router = express.Router();
const service = require('./deviceMeasurement.service');
//const deviceMeasurement = require('../models').deviceMeasurement;
const { check, validationResult } = require("express-validator");
const { Op } = require("sequelize");

// routes
router.post('/', createData);
router.get('/', getDeviceMeasurements);
router.get('/:id', getDeviceMeasurementsById);
router.put('/:id', updateData);
router.delete('/:id', deleteData);
router.get('/get/devices', getDevices);
router.get('/get/measurements', getMeasurements);

module.exports = router;

function createData(req, res, next) {
    service.createData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDeviceMeasurements(req, res, next) {
    service.getDeviceMeasurements(req.query)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDeviceMeasurementsById(req, res, next) {
    service.getDeviceMeasurementsById(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function updateData(req, res, next) {
    service.updateData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function deleteData(req, res, next) {
    service.deleteData(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDevices(req, res, next) {
    service.getDevices()
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getMeasurements(req, res, next) {
    service.getMeasurements()
        .then(result => res.json(result))
        .catch(err => next(err));
}