﻿const express = require('express');
const router = express.Router();
const service = require('./interfacecard.service');
const InterfaceCard = require('../models').InterfaceCard;
const { check, validationResult } = require("express-validator");
const { Op } = require("sequelize");

// routes
router.post('/', [
    check('givenInterfaceCardId', 'Interface Card Id cannot be left blank').trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            InterfaceCard.findOne({ 
                where: { 
                    givenInterfaceCardId: value,
                    assetId: req.body.assetId
                } 
            }).then(result => {
                if(result) return reject(new Error('This Interface Card Id is already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
], createData);
router.get('/', getData);
router.get('/:id', getDataById);
router.put('/:id', [
    check('givenInterfaceCardId', 'Interface Card Id cannot be left blank').trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            InterfaceCard.findOne({ 
                where: { 
                    givenInterfaceCardId: value,
                    assetId: req.body.assetId,
                    id: {
                        [Op.ne]: req.body.id
                    }
                } 
            }).then(result => {
                if(result) return reject(new Error('This Interface Card Id is already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
], updateData);
router.delete('/:id', deleteData);
router.get('/get/clients', getClients);
router.get('/get/assets', getAssets);
router.get('/get/interfacecardtypes', getInterfaceCardtypes);

module.exports = router;

function createData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }

    service.createData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getData(req, res, next) {
    service.getData(req.query)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDataById(req, res, next) {
    service.getDataById(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function updateData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }
    
    service.updateData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function deleteData(req, res, next) {
    service.deleteData(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getClients(req, res, next) {
    service.getClients()
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getAssets(req, res, next) {
    service.getAssets()
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getInterfaceCardtypes(req, res, next) {
    service.getInterfaceCardtypes()
        .then(result => res.json(result))
        .catch(err => next(err));
}