﻿const { Op } = require("sequelize");
const Modal = require('../models').InterfaceCard;
const InterfacecardType = require('../models').InterfacecardType;

const getData = async (params) => {
    const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

    let whereObject = {};
    whereObject.status = {
        [Op.ne]: 'deleted'
    };

    if(searchTerm) {
        whereObject.cardName = {
            [Op.iLike]: `%${searchTerm}%`
        }
    }

    let sortOrder = [];
    if(sortColumn && sortDirection) {
        sortOrder = [[sortColumn, sortDirection]]
    } else {
        sortOrder = [['id', 'desc']]
    }

    return { count, rows } = await Modal.findAndCountAll({
        where: whereObject,
        include: ['asset'],
        // include: [ { model: Modal, as: 'ancestors' } ],
        offset: (page - 1) * pageSize,
        limit: pageSize,
        order: sortOrder
    });
};

const createData = async (data, currentUserId) => {
    const date = `${data.installationDate.year}-${('0' + data.installationDate.month).slice(-2)}-${('0' + data.installationDate.day).slice(-2)}`;
    data.installationDate = date;
    data.createdBy = data.updateBy = currentUserId;

    if(data.formAddAttributes) {
        data.addAttributes = data.formAddAttributes;
    }

    const modal = await Modal.create(data);
    return modal;
}

const getDataById = async (id) => {
    const result = await Modal.findByPk(id);
    return result;
};

const updateData = async (data, currentUserId) => {
    const date = `${data.installationDate.year}-${('0' + data.installationDate.month).slice(-2)}-${('0' + data.installationDate.day).slice(-2)}`;
    data.installationDate = date;
    data.updateBy = currentUserId;

    if(data.formAddAttributes) {
        data.addAttributes = JSON.stringify(data.formAddAttributes);
    }

    await Modal.update(data, {
        where: {
          id: data.id
        }
    });
    return true;
}

const deleteData = async (id) => {
    await Modal.update({'status': 'deleted'}, {
        where: {
          id: id
        }
    });
    return true;
}

const getAssets = async () => {
    const results = await Modal.findAll({
        where: {
            status: 'enable'
        },
        include: [ { model: Modal, as: 'ancestors' } ],
        //order: [ [ { model: Modal, as: 'ancestors' }, 'hierarchyLevel', ] ]
    });

    return results;
};

const getInterfaceCardtypes = async () => {
    const results = await InterfacecardType.findAll({
        where: {
            status: 'enable'
        },
    });

    return results;
};

module.exports = {
    getData,
    createData,
    getDataById,
    updateData,
    deleteData,
    getAssets,
    getInterfaceCardtypes
};