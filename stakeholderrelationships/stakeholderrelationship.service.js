﻿const { Op } = require("sequelize");
const mStakeholderRelationship = require("../models").mStakeholderRelationship;
const Modal = require("../models").rStakeholderRelationship;
const Stakeholder = require("../models").Stakeholder;

const getData = async (params) => {
  const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

  let whereObject = {};
  whereObject.status = {
    [Op.ne]: "deleted",
  };

  if (searchTerm) {
    whereObject.assetName = {
      [Op.iLike]: `%${searchTerm}%`,
    };
  }

  let sortOrder = [];
  if (sortColumn && sortDirection) {
    sortOrder = [[sortColumn, sortDirection]];
  } else {
    sortOrder = [["id", "desc"]];
  }

  return ({ count, rows } = await Modal.findAndCountAll({
    where: whereObject,
    include: ["relationship", "leftstakeholder", "rightstakeholder"],
    // include: [{ model: Modal, as: "ancestors" }],
    offset: (page - 1) * pageSize,
    limit: pageSize,
    order: sortOrder,
  }));
};

const createData = async (data, currentUserId) => {
  data.createdBy = data.updatedBy = currentUserId;

  const modal = await Modal.create(data);
  return modal;
};

const getDataById = async (id) => {
  const result = await Modal.findByPk(id);
  return result;
};

const updateData = async (data, currentUserId) => {
  data.updatedBy = currentUserId;

  await Modal.update(data, {
    where: {
      id: data.id,
    },
  });
  return true;
};

const deleteData = async (id) => {
  await Modal.update(
    { status: "deleted" },
    {
      where: {
        id,
      },
    }
  );
  return true;
};

const getRelationships = async () => {
  const results = await mStakeholderRelationship.findAll({
    where: {
      status: "active",
    },
  });

  return results;
};

const getStakeholders = async () => {
  const results = await Stakeholder.findAll({
    where: {
      status: "enable",
    },
  });

  return results;
};

module.exports = {
  getData,
  createData,
  getDataById,
  updateData,
  deleteData,
  getRelationships,
  getStakeholders,
};
