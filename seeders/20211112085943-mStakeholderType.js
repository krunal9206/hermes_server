'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('mStakeholderTypes', [{
      stakeholderName : "Business Owner",
      description : "Hermes Technologies",
      createdBy : "Dipen Maniar",
      updatedBy : "Dipen Maniar",
      createdAt : new Date(),
      updatedAt : new Date()
    }]);
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('mStakeholderTypes', null, {});
  }
};