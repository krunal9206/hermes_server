﻿const { Op } = require("sequelize");
//const { noExtendLeft } = require("sequelize/types/lib/operators");
const DeviceType = require("../models").DeviceType;
const device = require('../../hermespersistence/models').device;
const deviceMeasurement = require('../../hermespersistence/models').deviceMeasurement;
const isEmpty = require('../_helpers/emptyObjectCheck');

const getData = async (params) => {
  const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

  let whereObject = {};
  whereObject.status = {
    [Op.ne]: "deleted",
  };

  if (searchTerm) {
    whereObject.deviceName = {
      [Op.iLike]: `%${searchTerm}%`,
    };
  }

  let sortOrder = [];
  if (sortColumn && sortDirection) {
    sortOrder = [[sortColumn, sortDirection]];
  } else {
    sortOrder = [["id", "desc"]];
  }

  return ({ count, rows } = await Modal.findAndCountAll({
    where: whereObject,
    include: ["interfacecard"],
    offset: (page - 1) * pageSize,
    limit: pageSize,
    order: sortOrder,
  }));
};

const getDevices = async (req) => {
  
  let whereObject = {};
  whereObject.active = {
    [Op.eq]: true,
  };

  if (req.query.searchTerm) {
    whereObject.modelName = {
      [Op.iLike]: `%${req.query.searchTerm}%`,
    };
  }

  let sortOrder = [];
  if (req.query.sortColumn && req.query.sortDirection) {
    sortOrder = [[req.query.sortColumn, req.query.sortDirection]];
  } else {
    sortOrder = [["modelName", "DESC"]];
  }

  return ({ count, rows } = await device.findAndCountAll({
    where: whereObject,
    //include: ["interfacecard"],
    offset: (req.query.page - 1) * req.query.pageSize,
    limit: req.query.pageSize,
    order: sortOrder,
  }));
};

const createData = async (data, currentUserId) => {
  const date = `${data.installationDate.year}-${(
    "0" + data.installationDate.month
  ).slice(-2)}-${("0" + data.installationDate.day).slice(-2)}`;
  data.installationDate = date;
  data.createdBy = data.updateBy = currentUserId;

  const modal = await Modal.create(data);
  return modal;
};

const getDeviceById = async (id) => {
  return await device.findByPk(id); 
};


const getDeviceAndMeasurementsById = async (id) => {
  return await device.findByPk(id, {    
    include: deviceMeasurement
  }); 
};

const calculateDeviceAndMeasurementsById = async(req) => {
  
  const deviceResult = await device.findByPk(req.params.id, {    
    include: deviceMeasurement
  });
  let measurements = []
  if (deviceResult.isActive()) {
    deviceResult.deviceMeasurements.forEach(deviceMeasurement => {
      //Unit to be added later
      if (deviceMeasurement.isActive()) {
        const devMeasurementRes = deviceMeasurement.calculate(JSON.parse(req.query.scope));
        console.log(`devMeasurementRes : ${devMeasurementRes}`);
        let calcTelemetry = {};
        calcTelemetry["sName"] = deviceMeasurement.shortMeasurementName;
        calcTelemetry["value"] = devMeasurementRes;
        //calcTelemetry[deviceMeasurement.shortMeasurementName] = devMeasurementRes;
        console.log(`calcTelemetry() : ${JSON.stringify(calcTelemetry, null, 2)}`);
        measurements.push(calcTelemetry);
      }
      else console.log(`Device model ${deviceResult.modelName}, measurement ${deviceMeasurement.shortMeasurementName} is not active`);   
    });
  }
  console.log(`id : ${req.params.id}, scope : ${req.query.scope}, measurements : ${[...measurements]}`);
  return isEmpty(measurements)? null : measurements;  
  //Add unit calculation later.
}

const updateData = async (data, currentUserId) => {
  const date = `${data.installationDate.year}-${(
    "0" + data.installationDate.month
  ).slice(-2)}-${("0" + data.installationDate.day).slice(-2)}`;
  data.installationDate = date;
  data.updateBy = currentUserId;

  await Modal.update(data, {
    where: {
      id: data.id,
    },
  });
  return true;
};

const deleteData = async (id) => {
  await Modal.update(
    { status: "deleted" },
    {
      where: {
        id: id,
      },
    }
  );
  return true;
};

const getInterfaceCards = async () => {
  const results = await InterfaceCard.findAll({
    where: {
      status: "enable",
    },
  });

  return results;
};

const getDevicetypes = async () => {
  const results = await DeviceType.findAll({
    where: {
      status: "enable",
    },
  });

  return results;
};

module.exports = {
  getDevices,
  getData,
  createData,
  getDeviceById,
  getDeviceAndMeasurementsById,
  calculateDeviceAndMeasurementsById,
  updateData,
  deleteData,
  getInterfaceCards,
  getDevicetypes,
};
