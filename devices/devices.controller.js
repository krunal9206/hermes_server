﻿const express = require('express');
const router = express.Router();
const service = require('./device.service');
const device = require('../../hermespersistence/models').device;
const { check, validationResult } = require("express-validator");
const { Op } = require("sequelize");

// routes
router.post('/', [
    check('givenDeviceId', 'Device Id cannot be left blank').trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            device.findOne({ 
                where: { 
                    givenDeviceId: value,
                    interfaceCardId: req.body.interfaceCardId
                } 
            }).then(result => {
                if(result) return reject(new Error('This Device Id is already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
], createData);
//router.get('/', getData);
//router.get('*', setFilterPaginationSortParams);
router.get('/', getDevices)
router.get('/:id', getDeviceById);
router.get('/:id/deviceMeasurements', getDeviceAndMeasurementsById);
router.get('/:id/calculateDeviceMeasurements', calculateDeviceAndMeasurementsById);
router.put('/:id', [
    check('givenDeviceId', 'Device Id cannot be left blank').trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            device.findOne({ 
                where: { 
                    givenDeviceId: value,
                    interfaceCardId: req.body.interfaceCardId,
                    id: {
                        [Op.ne]: req.body.id
                    }
                } 
            }).then(result => {
                if(result) return reject(new Error('This Device Id is already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
], updateData);
router.delete('/:id', deleteData);
router.get('/get/interfacecards', getInterfaceCards);
router.get('/get/devicetypes', getDevicetypes);

module.exports = router;

function createData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }

    service.createData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getData(req, res, next) {
    service.getData(req.query)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDeviceById(req, res, next) {
    service.getDeviceById(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function updateData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }
    
    service.updateData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function deleteData(req, res, next) {
    service.deleteData(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getInterfaceCards(req, res, next) {
    service.getInterfaceCards()
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDevicetypes(req, res, next) {
    service.getDevicetypes()
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDevices(req, res, next) {
    service.getDevices(req)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDeviceAndMeasurementsById(req, res, next) {
    service.getDeviceAndMeasurementsById(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function calculateDeviceAndMeasurementsById(req, res, next) {
    service.calculateDeviceAndMeasurementsById(req)
        .then(result => res.json(result))
        .catch(err => next(err));
}