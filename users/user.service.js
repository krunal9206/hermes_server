﻿const config = require('config.json');
const jwt = require('jsonwebtoken');
const { Op } = require("sequelize");
const User = require('../models').User;
const Helper = require('../_helpers/helper');
//const { use } = require('./users.controller');
const Role = require('../models').Role;
const Asset = require('../models').Asset;

async function authenticate({ username, password, side }) {
    const user = await User.scope('withPassword').findOne({
        where: {
            email: username,
            //password: password
        }
    });

    if (user) {
        if(Helper.comparePassword(user.password, password)) {
            const token = jwt.sign({ user_id: user.id }, config.secret);
            delete user.dataValues.password;

            let assestIds = [];
            if(side == 'client') {
                const assests = await Asset.findAll({
                    where: {
                        status: 'enable', 
                        stakeholderId: user.stakeholderId
                    }
                });

                console.log(assests);
                assestIds = assests.map(item => item.id)
            }

            return {
                user,
                token,
                assestIds
            };
        }
    }
}

const getData = async (params, currentUserId) => {
    const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

    let whereObject = {};
    whereObject.status = {
        [Op.ne]: 'deleted'
    };

    whereObject.id = {
        [Op.ne]: currentUserId
    };

    if(searchTerm) {
        whereObject.firstName = {
            [Op.iLike]: `%${searchTerm}%`
        }
    }

    let sortOrder = [];
    if(sortColumn && sortDirection) {
        sortOrder = [[sortColumn, sortDirection]]
    } else {
        sortOrder = [['id', 'desc']]
    }

    return { count, rows } = await User.findAndCountAll({
        where: whereObject,
        // include : [{model: UserRole, as: "userrole", include :[{model : Role, as: "role"}]}],
        include : [],
        distinct: true, 
        offset: (page - 1) * pageSize,
        limit: pageSize,
        order: sortOrder
    });
};

const createData = async (data) => {
    data.password = Helper.hashPassword(data.password);
    const user = await User.create(data);
    // for (const role of data.userRoles) {
    //     await UserRole.create({ userId: user.id, roleId: role.roleId, startDate: role.startDate, endDate: role.endDate });
    // }
    return user;
};

const getDataById = async (id) => {
    const result = await User.findByPk(id, {include: ['userrole'],});
    return result;
};

const updateData = async (data) => {
    if(data.password !== "") {
        data.password = Helper.hashPassword(data.password);
    } else {
        delete data.password;
    }

    await User.update(data, {
        where: {
          id: data.id
        }
    });

    // for (const role of data.userRoles) {
    //     if(role.id > 0) {
    //         await UserRole.update(
    //             { roleId: role.roleId, startDate: role.startDate, endDate: role.endDate },
    //             {
    //                 where: {
    //                     id: role.id
    //                 }
    //             }
    //         );
    //     } else {
    //         await UserRole.create({ userId: data.id, roleId: role.roleId, startDate: role.startDate, endDate: role.endDate });
    //     }
    // }
    
    return true;
}

const deleteData = async (id) => {
    await User.update({'status': 'deleted'}, {
        where: {
          id: id
        }
    });
    return true;
}

const getRoles = async () => {
    const roles = await Role.findAll({
        where: {
            status: 'enable'
        }
    });

    return roles;
};

module.exports = {
    authenticate,
    getData,
    createData,
    getDataById,
    updateData,
    deleteData,
    getRoles
};