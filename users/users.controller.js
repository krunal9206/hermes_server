﻿const express = require('express');
const router = express.Router();
const service = require('./user.service');
const User = require('../models').User;
const { check, validationResult } = require("express-validator");
const { Op } = require("sequelize");

// routes
router.post('/authenticate', authenticate);
router.post('/', [
    check('email').isEmail().withMessage('Please enter a valid email address').trim().custom(value => {
        return new Promise((resolve, reject) => {
            User.findOne({ where: { email: value } }).then(user => {
                if(user) return reject(new Error('This email already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
    check('givenUserId', 'User Id cannot be left blank').trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            User.findOne({ 
                where: { 
                    givenUserId: value,
                } 
            }).then(result => {
                if(result) return reject(new Error('This User Id is already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
], createData);
router.get('/', getData);
router.get('/:id', getDataById);
router.put('/:id', [
    check('email').isEmail().withMessage('Please enter a valid email address').trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            User.findOne({
                where: { 
                    email: value,
                    id: {
                        [Op.ne]: req.body.id
                    } 
                } 
            }).then(user => {
                if(user) return reject(new Error('This email already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
    check('givenUserId', 'User Id cannot be left blank').trim().custom((value, {req}) => {
        return new Promise((resolve, reject) => {
            User.findOne({ 
                where: { 
                    givenUserId: value,
                    id: {
                        [Op.ne]: req.body.id
                    }
                } 
            }).then(result => {
                if(result) return reject(new Error('This User Id is already exists.'))
                else return resolve(value)
            }).catch(error => {
                return reject(err)
            });
        })
    }),
], updateData);
router.delete('/:id', deleteData);
router.get('/get/roles', getRoles);

module.exports = router;

function authenticate(req, res, next) {
    service.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Email or password is incorrect' }))
        .catch(err => next(err));
}

function createData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }
    
    service.createData(req.body)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getData(req, res, next) {
    service.getData(req.query, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDataById(req, res, next) {
    service.getDataById(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function updateData(req, res, next) {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).json({message : errors.array()[0]['msg']});
    }
    service.updateData(req.body)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function deleteData(req, res, next) {
    service.deleteData(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getRoles(req, res, next) {
    service.getRoles()
        .then(result => res.json(result))
        .catch(err => next(err));
}
