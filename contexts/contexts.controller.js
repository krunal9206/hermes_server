﻿const express = require('express');
const router = express.Router();
const contextService = require('./context.service');

// routes
router.post('/', createData);
router.get('/', getData);
router.get('/:id', getDataById);
router.put('/:id', updateData);
router.delete('/:id', deleteData);

module.exports = router;

function createData(req, res, next) {
    contextService.createData(req.body)
        .then(context => res.json(context))
        .catch(err => next(err));
}

function getData(req, res, next) {
    contextService.getData(req.query)
        .then(context => res.json(context))
        .catch(err => next(err));
}

function getDataById(req, res, next) {
    contextService.getDataById(req.params.id)
        .then(context => res.json(context))
        .catch(err => next(err));
}

function updateData(req, res, next) {
    contextService.updateData(req.body)
        .then(context => res.json(context))
        .catch(err => next(err));
}

function deleteData(req, res, next) {
    contextService.deleteData(req.params.id)
        .then(context => res.json(context))
        .catch(err => next(err));
}