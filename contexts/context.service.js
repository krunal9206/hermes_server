﻿const { Op } = require("sequelize");
const Context = require('../models').Context;

const getData = async (params) => {
    const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

    let whereObject = {};
    whereObject.status = {
        [Op.ne]: 'deleted'
    };

    if(searchTerm) {
        whereObject.contextName = {
            [Op.iLike]: `%${searchTerm}%`
        }
    }

    let sortOrder = [];
    if(sortColumn && sortDirection) {
        sortOrder = [[sortColumn, sortDirection]]
    } else {
        sortOrder = [['id', 'desc']]
    }

    return { count, rows } = await Context.findAndCountAll({
        where: whereObject,
        offset: (page - 1) * pageSize,
        limit: pageSize,
        order: sortOrder
    });
};

const createData = async (data) => {
    const context = await Context.create(data);
    return context;
}

const getDataById = async (id) => {
    const context = await Context.findByPk(id);
    return context;
};

const updateData = async (data) => {
    await Context.update(data, {
        where: {
          id: data.id
        }
    });
    return true;
}

const deleteData = async (id) => {
    await Context.update({'status': 'deleted'}, {
        where: {
          id: id
        }
    });
    return true;
}

module.exports = {
    getData,
    createData,
    getDataById,
    updateData,
    deleteData
};