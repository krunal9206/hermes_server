﻿const express = require('express');
const router = express.Router();
const service = require('./role.service');

// routes
router.post('/', createData);
router.get('/', getData);
router.get('/:id', getDataById);
router.put('/:id', updateData);
router.delete('/:id', deleteData);
router.get('/get/contexts', getContexts);

module.exports = router;

function createData(req, res, next) {
    service.createData(req.body)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getData(req, res, next) {
    service.getData(req.query)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDataById(req, res, next) {
    service.getDataById(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function updateData(req, res, next) {
    service.updateData(req.body)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function deleteData(req, res, next) {
    service.deleteData(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getContexts(req, res, next) {
    service.getContexts()
        .then(result => res.json(result))
        .catch(err => next(err));
}