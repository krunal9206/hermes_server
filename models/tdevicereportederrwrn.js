'use strict';
module.exports = (sequelize, DataTypes) => {
  const tDeviceReportedErrWrn = sequelize.define('tDeviceReportedErrWrn', {
    systemErrWrnTxnId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    givenStakeholderId: DataTypes.STRING,
    givenParentDeviceInstanceId: DataTypes.STRING,
    givenDeviceInstanceId: DataTypes.STRING,
    givenDeviceErrWrnCode: DataTypes.STRING,
    generatedDateTime: DataTypes.BIGINT,
    resolvedDateTime: DataTypes.BIGINT,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
  }, {});
  tDeviceReportedErrWrn.associate = function(models) {
    // associations can be defined here
    tDeviceReportedErrWrn.belongsTo(models.mDeviceInstance, { foreignKey:'givenDeviceInstanceId', targetKey: 'givenDeviceInstanceId', as: 'DeviceInstance'} );
    tDeviceReportedErrWrn.belongsTo(models.mDeviceErrWrnCode, { foreignKey:'givenDeviceErrWrnCode', targetKey: 'givenDeviceErrWrnCode', as: 'DeviceErrWrnCode'} );
  };
  return tDeviceReportedErrWrn;
};