"use strict";
module.exports = (sequelize, DataTypes) => {
  const mAssetType = sequelize.define(
    "mAssetType",
    {
      systemAssetTypeId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      assetTypeName: DataTypes.STRING,
      description: DataTypes.TEXT,
      addlAttributesCollectionTemplate: DataTypes.TEXT,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  mAssetType.associate = function (models) {
    // associations can be defined here
  };
  return mAssetType;
};
