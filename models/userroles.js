'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserRoles = sequelize.define('UserRoles', {
    userId: DataTypes.INTEGER,
    roleId: DataTypes.INTEGER,
    startDate: DataTypes.DATEONLY,
    endDate: DataTypes.DATEONLY
  }, {});
  UserRoles.associate = function(models) {
    // associations can be defined here
    UserRoles.belongsTo(models.Role, { foreignKey:'roleId', as: 'role'} );
  };
  return UserRoles;
};