"use strict";
module.exports = (sequelize, DataTypes) => {
  const tDeviceCommands = sequelize.define(
    "tDeviceCommands",
    {
      serverTxnId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      givenStakeholderId: DataTypes.STRING,
      givenDeviceInstanceId: DataTypes.STRING,
      commandId: DataTypes.STRING,
      clientTxnId: DataTypes.STRING,
      clientTxnSentDateTime: DataTypes.BIGINT,
      clientTxnCaptureTime: DataTypes.BIGINT,
      responseCode: DataTypes.INTEGER,
      measurementJSONSet: DataTypes.JSONB,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  tDeviceCommands.associate = function (models) {
    // associations can be defined here
  };
  return tDeviceCommands;
};
