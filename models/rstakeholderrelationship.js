"use strict";
module.exports = (sequelize, DataTypes) => {
  const rStakeholderRelationship = sequelize.define(
    "rStakeholderRelationship",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      leftSystemStakeholderId: DataTypes.INTEGER,
      relationshipId: DataTypes.INTEGER,
      rightSystemStakeholderId: DataTypes.INTEGER,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  rStakeholderRelationship.associate = function (models) {
    // associations can be defined here
    rStakeholderRelationship.belongsTo(models.mStakeholderRelationship, {
      foreignKey: "relationshipId",
      as: "relationship",
    });
    rStakeholderRelationship.belongsTo(models.Stakeholder, {
      foreignKey: "leftSystemStakeholderId",
      as: "leftstakeholder",
    });
    rStakeholderRelationship.belongsTo(models.Stakeholder, {
      foreignKey: "rightSystemStakeholderId",
      as: "rightstakeholder",
    });
  };
  return rStakeholderRelationship;
};
