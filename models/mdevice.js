"use strict";
module.exports = (sequelize, DataTypes) => {
  const mDevice = sequelize.define(
    "mDevice",
    {
      systemDeviceId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      systemDeviceTypeId: DataTypes.INTEGER,
      systemManufacturerId: DataTypes.INTEGER,
      deviceModel: DataTypes.STRING,
      status: DataTypes.STRING,
      addlAttributesCollectionTemplate: DataTypes.TEXT,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  mDevice.associate = function (models) {
    // associations can be defined here
    mDevice.belongsTo(models.mDeviceType, {
      foreignKey: "systemDeviceTypeId",
      as: "devicetype",
    });
  };
  return mDevice;
};
