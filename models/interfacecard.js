'use strict';
module.exports = (sequelize, DataTypes) => {
  const InterfaceCard = sequelize.define('InterfaceCard', {
    assetId: DataTypes.INTEGER,
    interfaceCardTypeId: DataTypes.INTEGER,
    givenInterfaceCardId: DataTypes.STRING,
    cardName: DataTypes.STRING,
    description: DataTypes.TEXT,
    installationDate: DataTypes.DATEONLY,
    serialNo: DataTypes.STRING,
    latitude: DataTypes.STRING,
    longitude: DataTypes.STRING,
    addAttributes: DataTypes.TEXT,
    status: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updateBy: DataTypes.INTEGER
  }, {});
  InterfaceCard.associate = function(models) {
    // associations can be defined here
    InterfaceCard.belongsTo(models.Asset, { foreignKey:'assetId', as: 'asset'} );
  };
  return InterfaceCard;
};