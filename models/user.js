'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    givenUserId: DataTypes.STRING,
    stakeholderId: DataTypes.INTEGER,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    status: DataTypes.STRING,
  }, {
    defaultScope: {
      attributes: { exclude: ['password'] },
    },
    scopes: {
      withPassword: {
          attributes: { },
      }
    }
  });
  User.associate = function(models) {
    // associations can be defined here
    User.hasMany(models.UserRoles, { foreignKey:'userId', as: 'userrole'} );
  };
  return User;
};