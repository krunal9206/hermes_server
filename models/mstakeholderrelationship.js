"use strict";
module.exports = (sequelize, DataTypes) => {
  const mStakeholderRelationship = sequelize.define(
    "mStakeholderRelationship",
    {
      relationshipId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      relationshipName: DataTypes.STRING,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  mStakeholderRelationship.associate = function (models) {
    // associations can be defined here
  };
  return mStakeholderRelationship;
};
