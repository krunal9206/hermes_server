'use strict';
module.exports = (sequelize, DataTypes) => {
  const StakeholderType = sequelize.define('StakeholderType', {
    givenStakeholderTypeId: DataTypes.STRING,
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    status: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, {});
  StakeholderType.associate = function(models) {
    // associations can be defined here
  };
  return StakeholderType;
};

