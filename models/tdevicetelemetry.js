"use strict";
module.exports = (sequelize, DataTypes) => {
  const tDeviceTelemetry = sequelize.define(
    "tDeviceTelemetry",
    {
      serverTxnId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      givenStakeholderId: DataTypes.STRING,
      givenDeviceInstanceId: DataTypes.STRING,
      clientTxnSentDateTime: DataTypes.BIGINT,
      clientTxnCaptureTime: DataTypes.BIGINT,
      clientSoftwareVersion: DataTypes.STRING,
      clientDataFormatVersion: DataTypes.STRING,
      measurementJSONSet: DataTypes.JSONB,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  tDeviceTelemetry.associate = function (models) {
    // associations can be defined here
  };
  return tDeviceTelemetry;
};
