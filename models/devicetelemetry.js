'use strict';
module.exports = (sequelize, DataTypes) => {
  const DeviceTelemetry = sequelize.define('DeviceTelemetry', {
    givenClientId: DataTypes.STRING,
    givenInterfaceCardId: DataTypes.STRING,
    givenDeviceId: DataTypes.STRING,
    clientTxnDateTime: DataTypes.STRING,
    shortMeasurementName: DataTypes.STRING,
    deviceValue: DataTypes.STRING,
    deviceValueUnit: DataTypes.STRING,
    deviceTime: DataTypes.STRING,
    addlJson: DataTypes.TEXT,
    status: DataTypes.STRING,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {});
  DeviceTelemetry.associate = function(models) {
    // associations can be defined here
  };
  return DeviceTelemetry;
};