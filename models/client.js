'use strict';
module.exports = (sequelize, DataTypes) => {
  const Client = sequelize.define('Client', {
    givenClientId: DataTypes.STRING,
    clientName: DataTypes.STRING,
    description: DataTypes.TEXT,
    activationDateTime: DataTypes.DATE,
    createdBy: DataTypes.INTEGER,
    updateBy: DataTypes.INTEGER,
    status: DataTypes.STRING
  }, {});
  Client.associate = function(models) {
    // associations can be defined here
  };
  return Client;
};