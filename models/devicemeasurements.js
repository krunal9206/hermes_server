'use strict';
module.exports = (sequelize, DataTypes) => {
  const DeviceMeasurements = sequelize.define('DeviceMeasurements', {
    deviceId: DataTypes.INTEGER,
    measurementId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    status: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updateBy: DataTypes.INTEGER
  }, {});
  DeviceMeasurements.associate = function(models) {
    // associations can be defined here
  };
  return DeviceMeasurements;
};