'use strict';
module.exports = (sequelize, DataTypes) => {
  const DeviceType = sequelize.define('DeviceType', {
    givenDeviceTypeId: DataTypes.STRING,
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    status: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, {});
  DeviceType.associate = function(models) {
    // associations can be defined here
  };
  return DeviceType;
};