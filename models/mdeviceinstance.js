"use strict";
module.exports = (sequelize, DataTypes) => {
  const mDeviceInstance = sequelize.define(
    "mDeviceInstance",
    {
      systemDeviceInstanceId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      givenDeviceInstanceId: DataTypes.STRING,
      systemDeviceId: DataTypes.INTEGER,
      systemAssetId: DataTypes.INTEGER,
      parentDeviceInstanceId: DataTypes.INTEGER,
      serialNo: DataTypes.STRING,
      productionDate: DataTypes.STRING,
      goLiveDate: DataTypes.STRING,
      addlAttributesSetupByAdmin: DataTypes.TEXT,
      addlAttributesSentByDevice: DataTypes.TEXT,
      latitude: DataTypes.STRING,
      longitude: DataTypes.STRING,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  mDeviceInstance.associate = function (models) {
    // associations can be defined here
  };
  return mDeviceInstance;
};
