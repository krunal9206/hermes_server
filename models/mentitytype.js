'use strict';
/* module.exports = (sequelize, DataTypes) => {
  const mStakeholderType = sequelize.define('mStakeholderType', {
    stakeholderId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    stakeholderName: DataTypes.STRING,
    description: DataTypes.STRING,
    status: {
      type: DataTypes.TEXT,
      defaultValue: 'Active',
      validate: {
        isIn: {
          args: [['Active', 'Inactive']],
          msg: "Must be Active or Inactive."
        }
      }
    },
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    paranoid: true
  });
  mStakeholderType.associate = function(models) {
    // associations can be defined here
  };
  return mStakeholderType;
}; */

const {
  Model
} = require('sequelize');
module.exports = (sequelize , DataTypes) => {
  class mEntityType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  mEntityType.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      unique: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    },
    createdBy: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    updatedBy: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
  }, {
    sequelize,
    modelName: 'mEntityType',
    paranoid: true,
  });
  return mEntityType;
};