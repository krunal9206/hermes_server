'use strict';
module.exports = (sequelize, DataTypes) => {
  const mDeviceErrWrnCode = sequelize.define('mDeviceErrWrnCode', {
    systemDeviceErrWrnId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    givenDeviceErrWrnCode: DataTypes.STRING,
    systemDeviceId: DataTypes.INTEGER,
    errWrnCodeType: DataTypes.ENUM("E", "W"),
    errWrnDescription: DataTypes.TEXT,
    status: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER,
  }, {});
  mDeviceErrWrnCode.associate = function(models) {
    // associations can be defined here
  };
  return mDeviceErrWrnCode;
};