"use strict";
module.exports = (sequelize, DataTypes) => {
  const mManufacturer = sequelize.define(
    "mManufacturer",
    {
      systemManufacturerId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      manufacturerName: DataTypes.STRING,
      address: DataTypes.TEXT,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  mManufacturer.associate = function (models) {
    // associations can be defined here
  };
  return mManufacturer;
};
