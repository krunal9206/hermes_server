"use strict";
module.exports = (sequelize, DataTypes) => {
  const mDeviceType = sequelize.define(
    "mDeviceType",
    {
      systemDeviceTypeId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      deviceTypeName: DataTypes.STRING,
      description: DataTypes.TEXT,
      status: DataTypes.STRING,
    },
    {}
  );
  mDeviceType.associate = function (models) {
    // associations can be defined here
  };
  return mDeviceType;
};
