"use strict";
module.exports = (sequelize, DataTypes) => {
  const rDeviceRelationship = sequelize.define(
    "rDeviceRelationship",
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      leftSystemDeviceInstanceId: DataTypes.INTEGER,
      relationshipId: DataTypes.INTEGER,
      rightSystemDeviceInstanceId: DataTypes.INTEGER,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  rDeviceRelationship.associate = function (models) {
    // associations can be defined here
    rDeviceRelationship.belongsTo(models.mDeviceRelationship, {
      foreignKey: "relationshipId",
      as: "relationship",
    });
    rDeviceRelationship.belongsTo(models.mDeviceInstance, {
      foreignKey: "leftSystemDeviceInstanceId",
      as: "leftdeviceinstance",
    });
    rDeviceRelationship.belongsTo(models.mDeviceInstance, {
      foreignKey: "rightSystemDeviceInstanceId",
      as: "rightdeviceinstance",
    });
  };
  return rDeviceRelationship;
};
