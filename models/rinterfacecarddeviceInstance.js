"use strict";
module.exports = (sequelize, DataTypes) => {
  const rInterfaceCardDeviceInstance = sequelize.define(
    "rInterfaceCardDeviceInstance",
    {
      interfaceCardId: DataTypes.INTEGER,
      deviceTypeId: DataTypes.INTEGER,
      givenDeviceId: DataTypes.STRING,
      deviceName: DataTypes.STRING,
      description: DataTypes.TEXT,
      installationDate: DataTypes.DATEONLY,
      serialNo: DataTypes.STRING,
      latitude: DataTypes.STRING,
      longitude: DataTypes.STRING,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updateBy: DataTypes.INTEGER,
    },
    {}
  );
  rInterfaceCardDeviceInstance.associate = function (models) {
    // associations can be defined here
    rInterfaceCardDeviceInstance.belongsTo(models.InterfaceCard, {
      foreignKey: "interfaceCardId",
      as: "interfacecard",
    });
  };
  return rInterfaceCardDeviceInstance;
};
