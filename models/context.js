'use strict';
module.exports = (sequelize, DataTypes) => {
  const Context = sequelize.define('Context', {
    contextName: DataTypes.STRING,
    description: DataTypes.TEXT,
    status: DataTypes.STRING
  }, {});
  Context.associate = function(models) {
    // associations can be defined here
  };
  return Context;
};