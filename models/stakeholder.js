"use strict";
module.exports = (sequelize, DataTypes) => {
  const Stakeholder = sequelize.define(
    "Stakeholder",
    {
      stakeholderTypeId: DataTypes.INTEGER,
      givenStakeholderId: DataTypes.STRING,
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      email: DataTypes.STRING,
      phone: DataTypes.STRING,
      activationDateTime: DataTypes.DATE,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  Stakeholder.associate = function (models) {
    // associations can be defined here
  };
  return Stakeholder;
};
