'use strict';
module.exports = (sequelize, DataTypes) => {
  const Measurement = sequelize.define('Measurement', {
    shortMeasurementName: DataTypes.STRING,
    measurementUnit: DataTypes.STRING,
    displayMeasurementName: DataTypes.STRING,
    status: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updateBy: DataTypes.INTEGER
  }, {});
  Measurement.associate = function(models) {
    // associations can be defined here
  };
  return Measurement;
};