"use strict";
module.exports = (sequelize, DataTypes) => {
  const mDeviceStatus = sequelize.define(
    "mDeviceStatus",
    {
      systemDeviceStatusId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      givenDeviceStatusId: DataTypes.STRING,
      systemDeviceId: DataTypes.INTEGER,
      deviceStatusDescription: DataTypes.STRING,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  mDeviceStatus.associate = function (models) {
    // associations can be defined here
  };
  return mDeviceStatus;
};
