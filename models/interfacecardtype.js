'use strict';
module.exports = (sequelize, DataTypes) => {
  const InterfacecardType = sequelize.define('InterfacecardType', {
    givenInterfaceCardTypeId: DataTypes.STRING,
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    addAttributes: DataTypes.TEXT,
    status: DataTypes.STRING,
    createdBy: DataTypes.INTEGER,
    updatedBy: DataTypes.INTEGER
  }, {});
  InterfacecardType.associate = function(models) {
    // associations can be defined here
  };
  return InterfacecardType;
};