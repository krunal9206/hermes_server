"use strict";
module.exports = (sequelize, DataTypes) => {
  const Asset = sequelize.define(
    "Asset",
    {
      givenAssetId: DataTypes.STRING,
      stakeholderId: DataTypes.INTEGER,
      systemAssetTypeId: DataTypes.INTEGER,
      parentId: {
        type: DataTypes.INTEGER,
        hierarchy: true,
      },
      hierarchyLevel: DataTypes.INTEGER,
      assetName: DataTypes.STRING,
      description: DataTypes.TEXT,
      address: DataTypes.TEXT,
      state: DataTypes.STRING,
      latitude: DataTypes.STRING,
      longitude: DataTypes.STRING,
      addlAttributesSetupByAdmin: DataTypes.TEXT,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updateBy: DataTypes.INTEGER,
    },
    {}
  );
  Asset.associate = function (models) {
    // associations can be defined here
  };
  return Asset;
};
