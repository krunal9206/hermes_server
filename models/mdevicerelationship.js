"use strict";
module.exports = (sequelize, DataTypes) => {
  const mDeviceRelationship = sequelize.define(
    "mDeviceRelationship",
    {
      relationshipId: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      relationshipName: DataTypes.STRING,
      status: DataTypes.STRING,
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {}
  );
  mDeviceRelationship.associate = function (models) {
    // associations can be defined here
  };
  return mDeviceRelationship;
};
