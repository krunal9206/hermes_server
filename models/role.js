'use strict';
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    contextId: DataTypes.INTEGER,
    roleName: DataTypes.STRING,
    description: DataTypes.TEXT,
    status: DataTypes.STRING
  }, {});
  Role.associate = function(models) {
    // associations can be defined here
    Role.belongsTo(models.Context, { foreignKey:'contextId', as: 'context'} );
  };
  return Role;
};