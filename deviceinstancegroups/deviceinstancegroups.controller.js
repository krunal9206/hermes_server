﻿const express = require("express");
const router = express.Router();
const service = require("./deviceinstancegroup.service");
//const deviceInstance = require("../../hermespersistence/models").deviceInstance;
/* const { check, validationResult } = require("express-validator");
const { Op } = require("sequelize"); */

// routes
router.get("/", getDeviceInstanceGroups);
router.get("/:id", getDeviceInstanceGroupById);
router.get("/:id/deviceInstances", getDvceInstancesFromGroupId);
router.get("/:id/deviceInstances/configs", getDvceInstancesAndConfigFromGroupId);
/* router.post(
  "/",
  [
    check("givenDeviceInstanceId", "DeviceInstance Id cannot be left blank")
      .trim()
      .custom((value, { req }) => {
        return new Promise((resolve, reject) => {
          deviceInstance
            .findOne({
              where: {
                givenDeviceInstanceId: value,
              },
            })
            .then((result) => {
              if (result)
                return reject(
                  new Error("This DeviceInstance Id is already exists.")
                );
              else return resolve(value);
            })
            .catch((error) => {
              return reject(error);
            });
        });
      }),
  ],
  createData
);
//router.get("/", getData);
router.get("/", getDeviceinstances);
router.get("/:id", getDeviceInstanceById);
router.put(
  "/:id",
  [
    check("givenDeviceInstanceId", "DeviceInstance Id cannot be left blank")
      .trim()
      .custom((value, { req }) => {
        return new Promise((resolve, reject) => {
          deviceInstance
            .findOne({
              where: {
                givenDeviceInstanceId: value,
                systemDeviceInstanceId: {
                  [Op.ne]: req.body.systemDeviceInstanceId,
                },
              },
            })
            .then((result) => {
              if (result)
                return reject(
                  new Error("This DeviceInstance Id is already exists.")
                );
              else return resolve(value);
            })
            .catch((error) => {
              return reject(error);
            });
        });
      }),
  ],
  updateData
);
router.delete("/:id", deleteData);
router.get("/get/devices", getDevices);
router.get("/get/assets", getAssets);
//router.get("/get/deviceinstances", getDeviceinstances);

module.exports = router;

function createData(req, res, next) {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ message: errors.array()[0]["msg"] });
  }

  service
    .createData(req.body, req.user.user_id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getData(req, res, next) {
  service
    .getData(req.query)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getDeviceInstanceById(req, res, next) {
  service
    .getDeviceInstanceById(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function updateData(req, res, next) {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ message: errors.array()[0]["msg"] });
  }

  service
    .updateData(req.body, req.user.user_id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function deleteData(req, res, next) {
  service
    .deleteData(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getDevices(req, res, next) {
  service
    .getDevices()
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getAssets(req, res, next) {
  service
    .getAssets()
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getDeviceinstances(req, res, next) {
    service
    .getDeviceInstances(req)
    .then((result) => res.json(result))
    .catch((err) => next(err));
} */

function getDeviceInstanceGroupById(req, res, next) {
  service
    .getDeviceInstanceGroupById(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}
function getDeviceInstanceGroups(req, res, next) {
  service
  .getDeviceInstanceGroups(req)
  .then((result) => res.json(result))
  .catch((err) => next(err));
}
function getDvceInstancesFromGroupId(req, res, next) {
  service
    .getDvceInstancesFromGroupId(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getDvceInstancesAndConfigFromGroupId(req, res, next) {
  service
  .getDvceInstancesAndConfigFromGroupId(req.params.id)
  .then((result) => res.json(result))
  .catch((err) => next(err));
}

module.exports = router;