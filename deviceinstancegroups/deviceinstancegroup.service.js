﻿const { Op, where } = require("sequelize");
const deviceInstanceGroup = require("../../hermespersistence/models").deviceInstanceGroup;
const deviceInstance = require('../../hermespersistence/models').deviceInstance;
const deviceInstanceGroupConfig = require('../../hermespersistence/models').deviceInstanceGroupConfig;

async function getDeviceInstanceGroups(req) {
  let whereObject = {};

  if (req.query.searchTerm) {
    whereObject.name = {
      [Op.iLike]: `%${req.query.searchTerm}%`,
    };
  }

  let sortOrder = [];
  if (req.query.sortColumn && req.query.sortDirection) {
    sortOrder = [[req.query.sortColumn, req.query.sortDirection]];
  } else {
    sortOrder = [["createdAt", "DESC"]];
  }

  return ({ count, rows } = await deviceInstanceGroup.findAndCountAll({
    where: whereObject,
    offset: (req.query.page - 1) * req.query.pageSize,
    limit: req.query.pageSize,
    order: sortOrder,
  }));

};  
const getDeviceInstanceGroupById = async (id) => {  
  return await deviceInstanceGroup.findByPk(id);
};

const getDvceInstancesFromGroupId = async (id) => {  
  return await deviceInstanceGroup.findByPk(id, {
    include: deviceInstance
  });
};

const getDvceInstancesAndConfigFromGroupId = async (id) => {  
  /* return await deviceInstanceGroup.findByPk(id, {
    include: {
        all: true
    }
  }); */
  return await deviceInstanceGroup.findByPk(id, {
    include: [{
      model: deviceInstance,
      through: {
          attributes: []
      }},{
      model: deviceInstanceGroupConfig
    }]
  });
};

module.exports = {
  getDeviceInstanceGroups,
  getDeviceInstanceGroupById,
  getDvceInstancesFromGroupId,
  getDvceInstancesAndConfigFromGroupId
  /* getData,
  createData,
  getDeviceInstanceById,
  updateData,
  deleteData,
  getDevices,
  getDeviceInstances, */
};

/* const getData = async (params) => {
  const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

  let whereObject = {};
  whereObject.status = {
    [Op.ne]: "deleted",
  };

  if (searchTerm) {
    whereObject.assetName = {
      [Op.iLike]: `%${searchTerm}%`,
    };
  }

  let sortOrder = [];
  if (sortColumn && sortDirection) {
    sortOrder = [[sortColumn, sortDirection]];
  } else {
    sortOrder = [["systemDeviceInstanceId", "desc"]];
  }

  return ({ count, rows } = await deviceGroup.findAndCountAll({
    where: whereObject,
    // include: ['context'],
    // include: [{ model: Modal, as: "ancestors" }],
    offset: (page - 1) * pageSize,
    limit: pageSize,
    order: sortOrder,
  }));
};

const createData = async (data, currentUserId) => {
  data.createdBy = data.updateBy = currentUserId;
  if (data.parentId == "") {
    data.parentId = null;
  }
  if (data.formAddAttributes) {
    data.addlAttributesSetupByAdmin = JSON.stringify(data.formAddAttributes);
  }
  const modal = await deviceGroup.create(data);
  return modal;
};

const getDeviceInstanceById = async (id) => {  
  console.log("getDeviceInstanceById : %s", id);
  return await deviceGroup.findByPk(id);
};

const updateData = async (data, currentUserId) => {
  data.updateBy = currentUserId;
  if (data.parentId == "") {
    data.parentId = null;
  }
  if (data.formAddAttributes) {
    data.addlAttributesSetupByAdmin = JSON.stringify(data.formAddAttributes);
  }
  await deviceGroup.update(data, {
    where: {
      systemDeviceInstanceId: data.systemDeviceInstanceId,
    },
  });
  return true;
};

const deleteData = async (id) => {
  await deviceGroup.update(
    { status: "deleted" },
    {
      where: {
        systemDeviceInstanceId: id,
      },
    }
  );
  return true;
};

const getDevices = async () => {
  const results = await mDevice.findAll({
    where: {
      status: "enable",
    },
  });

  return results;
};

const getDeviceInstances = async(req) => {
  
  let whereObject = {};

  if (req.query.searchTerm) {
    whereObject.serialNo = {
      [Op.iLike]: `%${req.query.searchTerm}%`,
    };
  }

  let sortOrder = [];
  if (req.query.sortColumn && req.query.sortDirection) {
    sortOrder = [[req.query.sortColumn, req.query.sortDirection]];
  } else {
    sortOrder = [["createdAt", "DESC"]];
  }

  return ({ count, rows } = await deviceGroup.findAndCountAll({
    where: whereObject,
    offset: (req.query.page - 1) * req.query.pageSize,
    limit: req.query.pageSize,
    order: sortOrder,
  }));

}; */

/* module.exports = {
  getData,
  createData,
  getDeviceInstanceById,
  updateData,
  deleteData,
  getDevices,
  getDeviceInstances,
}; */
