﻿const express = require("express");
const router = express.Router();
const service = require("./asset.service");
const Asset = require("../models").Asset;
const { check, validationResult } = require("express-validator");
const { Op } = require("sequelize");

// routes
router.post(
  "/",
  [
    check("givenAssetId", "Asset Id cannot be left blank")
      .trim()
      .custom((value, { req }) => {
        return new Promise((resolve, reject) => {
          Asset.findOne({
            where: {
              givenAssetId: value,
              stakeholderId: req.body.stakeholderId,
            },
          })
            .then((result) => {
              if (result)
                return reject(new Error("This Asset Id is already exists."));
              else return resolve(value);
            })
            .catch((error) => {
              return reject(err);
            });
        });
      }),
  ],
  createData
);
router.get("/", getData);
router.get("/:id", getDataById);
router.put(
  "/:id",
  [
    check("givenAssetId", "Asset Id cannot be left blank")
      .trim()
      .custom((value, { req }) => {
        return new Promise((resolve, reject) => {
          Asset.findOne({
            where: {
              givenAssetId: value,
              stakeholderId: req.body.stakeholderId,
              id: {
                [Op.ne]: req.body.id,
              },
            },
          })
            .then((result) => {
              if (result)
                return reject(new Error("This Asset Id is already exists."));
              else return resolve(value);
            })
            .catch((error) => {
              return reject(err);
            });
        });
      }),
  ],
  updateData
);
router.delete("/:id", deleteData);
router.get("/get/stakeholders", getStakeholders);
router.get("/get/assettypes", getAssetTypes);
router.get("/get/assets", getAssets);

module.exports = router;

function createData(req, res, next) {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ message: errors.array()[0]["msg"] });
  }

  service
    .createData(req.body, req.user.user_id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getData(req, res, next) {
  service
    .getData(req.query)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getDataById(req, res, next) {
  service
    .getDataById(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function updateData(req, res, next) {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ message: errors.array()[0]["msg"] });
  }

  service
    .updateData(req.body, req.user.user_id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function deleteData(req, res, next) {
  service
    .deleteData(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getStakeholders(req, res, next) {
  service
    .getStakeholders()
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getAssetTypes(req, res, next) {
  service
    .getAssetTypes()
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getAssets(req, res, next) {
  service
    .getAssets()
    .then((result) => res.json(result))
    .catch((err) => next(err));
}
