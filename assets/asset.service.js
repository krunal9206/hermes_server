﻿const { Op } = require("sequelize");
const Stakeholder = require("../models").Stakeholder;
const mAssetType = require("../models").mAssetType;
const Modal = require("../models").Asset;

const getData = async (params) => {
  const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

  let whereObject = {};
  whereObject.status = {
    [Op.ne]: "deleted",
  };

  if (searchTerm) {
    whereObject.assetName = {
      [Op.iLike]: `%${searchTerm}%`,
    };
  }

  let sortOrder = [];
  if (sortColumn && sortDirection) {
    sortOrder = [[sortColumn, sortDirection]];
  } else {
    sortOrder = [["id", "desc"]];
  }

  return ({ count, rows } = await Modal.findAndCountAll({
    where: whereObject,
    //include: ['context'],
    include: [{ model: Modal, as: "ancestors" }],
    offset: (page - 1) * pageSize,
    limit: pageSize,
    order: sortOrder,
  }));
};

const createData = async (data, currentUserId) => {
  data.createdBy = data.updateBy = currentUserId;
  if (data.parentId == "") {
    data.parentId = null;
  }
  if (data.formAddAttributes) {
    data.addlAttributesSetupByAdmin = JSON.stringify(data.formAddAttributes);
  }
  const modal = await Modal.create(data);
  return modal;
};

const getDataById = async (id) => {
  const result = await Modal.findByPk(id);
  return result;
};

const updateData = async (data, currentUserId) => {
  data.updateBy = currentUserId;
  if (data.parentId == "") {
    data.parentId = null;
  }
  if (data.formAddAttributes) {
    data.addlAttributesSetupByAdmin = JSON.stringify(data.formAddAttributes);
  }

  await Modal.update(data, {
    where: {
      id: data.id,
    },
  });
  return true;
};

const deleteData = async (id) => {
  await Modal.update(
    { status: "deleted" },
    {
      where: {
        id: id,
      },
    }
  );
  return true;
};

const getStakeholders = async () => {
  const results = await Stakeholder.findAll({
    where: {
      status: "enable",
    },
  });

  return results;
};

const getAssetTypes = async () => {
  const results = await mAssetType.findAll({
    where: {
      status: "enable",
    },
  });

  return results;
};

const getAssets = async () => {
  const results = await Modal.findAll({
    where: {
      status: "enable",
    },
    include: [{ model: Modal, as: "ancestors" }],
    //order: [ [ { model: Modal, as: 'ancestors' }, 'hierarchyLevel', ] ]
  });

  return results;
};

module.exports = {
  getData,
  createData,
  getDataById,
  updateData,
  deleteData,
  getStakeholders,
  getAssetTypes,
  getAssets,
};
