﻿const { Op } = require("sequelize");
const Modal = require('../models').InterfacecardType;

const getData = async (params) => {
    const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

    let whereObject = {};
    whereObject.status = {
        [Op.ne]: 'deleted'
    };

    if(searchTerm) {
        whereObject.name = {
            [Op.iLike]: `%${searchTerm}%`
        }
    }

    let sortOrder = [];
    if(sortColumn && sortDirection) {
        sortOrder = [[sortColumn, sortDirection]]
    } else {
        sortOrder = [['id', 'desc']]
    }

    return { count, rows } = await Modal.findAndCountAll({
        where: whereObject,
        // include: ['interfacecard'],
        offset: (page - 1) * pageSize,
        limit: pageSize,
        order: sortOrder
    });
};

const createData = async (data, currentUserId) => {
    data.createdBy = data.updatedBy = currentUserId;

    const modal = await Modal.create(data);
    return modal;
}

const getDataById = async (id) => {
    const result = await Modal.findByPk(id);
    return result;
};

const updateData = async (data, currentUserId) => {
    data.updatedBy = currentUserId;

    await Modal.update(data, {
        where: {
          id: data.id
        }
    });
    return true;
}

const deleteData = async (id) => {
    await Modal.update({'status': 'deleted'}, {
        where: {
          id: id
        }
    });
    return true;
}

module.exports = {
    getData,
    createData,
    getDataById,
    updateData,
    deleteData
};