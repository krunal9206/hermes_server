﻿const { Op, where } = require("sequelize");
const currTelemetry = require("../../hermespersistence/models").deviceCurrentTelemetryPayload;

const getCurrTelemetryBydvceInstsAndMeasurements = async (dvceInsts, measurements) => {
  return await currTelemetry.findAll({
    where: {
      deviceInstanceId: {
        [Op.in]: dvceInsts
      },
      sName: {
        [Op.in]: measurements
      }
    }
  })  
}

const getData = async (params) => {
  const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

  let whereObject = {};
  whereObject.status = {
    [Op.ne]: "deleted",
  };

  if (searchTerm) {
    whereObject.assetName = {
      [Op.iLike]: `%${searchTerm}%`,
    };
  }

  let sortOrder = [];
  if (sortColumn && sortDirection) {
    sortOrder = [[sortColumn, sortDirection]];
  } else {
    sortOrder = [["systemDeviceInstanceId", "desc"]];
  }

  return ({ count, rows } = await deviceInstance.findAndCountAll({
    where: whereObject,
    // include: ['context'],
    // include: [{ model: Modal, as: "ancestors" }],
    offset: (page - 1) * pageSize,
    limit: pageSize,
    order: sortOrder,
  }));
};

const createData = async (data, currentUserId) => {
  data.createdBy = data.updateBy = currentUserId;
  if (data.parentId == "") {
    data.parentId = null;
  }
  if (data.formAddAttributes) {
    data.addlAttributesSetupByAdmin = JSON.stringify(data.formAddAttributes);
  }
  const modal = await deviceInstance.create(data);
  return modal;
};

const updateData = async (data, currentUserId) => {
  data.updateBy = currentUserId;
  if (data.parentId == "") {
    data.parentId = null;
  }
  if (data.formAddAttributes) {
    data.addlAttributesSetupByAdmin = JSON.stringify(data.formAddAttributes);
  }
  await deviceInstance.update(data, {
    where: {
      systemDeviceInstanceId: data.systemDeviceInstanceId,
    },
  });
  return true;
};

const deleteData = async (id) => {
  await deviceInstance.update(
    { status: "deleted" },
    {
      where: {
        systemDeviceInstanceId: id,
      },
    }
  );
  return true;
};


module.exports = {
  getCurrTelemetryBydvceInstsAndMeasurements,
};
