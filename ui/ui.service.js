﻿const { Op } = require("sequelize");
const Asset = require('../models').Asset;
const InterfaceCard = require('../models').InterfaceCard;

const getAssets = async (clientId) => {
    const assets = await Asset.findAll({
        where: {
            status: 'enable',
            clientId: clientId
        }
    });

    const interfacecards = await getInterfaceCards(assets);

    const results = {assets, interfacecards}

    return results;
};

const getInterfaceCards = async (assests) => {
    const assestIds = assests.map((asset) => asset.id)

    const results = await InterfaceCard.findAll({
        where: {
            status: 'enable',
            assetId: {
                [Op.in] : assestIds
            }
        }
    });

    return results;
};

module.exports = {
    getAssets
};