﻿const express = require('express');
const router = express.Router();
const service = require('./ui.service');


router.get('/getprofile', getProfile);

module.exports = router;

function getProfile(req, res, next) {
    const clientId = req.query.client_id;
    console.log(clientId);
    service.getAssets(clientId)
        .then(result => res.json(result))
        .catch(err => next(err));
}