﻿const { Op } = require("sequelize");
const model = require('../../hermespersistence/models').deviceInstanceGroupTelemetryPayload;

const getData = async (params) => {
    const { page, pageSize, searchTerm, sortColumn, sortDirection } = params;

    let whereObject = {};
    whereObject.status = {
        [Op.ne]: 'deleted'
    };

    if(searchTerm) {
        whereObject.shortMeasurementName = {
            [Op.iLike]: `%${searchTerm}%`
        }
    }

    let sortOrder = [];
    if(sortColumn && sortDirection) {
        sortOrder = [[sortColumn, sortDirection]]
    } else {
        sortOrder = [['id', 'desc']]
    }

    return { count, rows } = await model.findAndCountAll({
        where: whereObject,
        // include: ['interfacecard'],
        offset: (page - 1) * pageSize,
        limit: pageSize,
        order: sortOrder
    });
};

const createData = async (data, currentUserId) => {
    console.log(`req body : ${JSON.stringify(data, null, 2)}`);
    data.createdBy = data.updatedBy = currentUserId;

    let result;
    try {
        result = await model.create(data);
        console.log(`result : ${JSON.stringify(result, null, 2)}`);        
    } catch (error) {
        /* console.error(error);
        console.log(`result : ${result}`); */
        throw new Error(error);
    }
    return result;
}

const getDataById = async (id) => {
    const result = await model.findByPk(id);
    return result;
};

const updateData = async (data, currentUserId) => {
    data.updateBy = currentUserId;

    await model.update(data, {
        where: {
          id: data.id
        }
    });
    return true;
}

const deleteData = async (id) => {
    await model.update({'status': 'deleted'}, {
        where: {
          id: id
        }
    });
    return true;
}

module.exports = {
    getData,
    createData,
    getDataById,
    updateData,
    deleteData,
};