﻿const express = require('express');
const router = express.Router();
const service = require('./deviceinstancegrouptelemetrypayload.service');

// routes
router.post('/', createData);
/* router.get('/', getData);
router.get('/:id', getDataById);
router.put('/:id', updateData);
router.delete('/:id', deleteData); */

module.exports = router;

function createData(req, res, next) {
    //console.log(`dvce grp post request is : ${JSON.stringify(req, null, 2)}`);
    //req.user.user_id = '00000000-0000-0000-0000-000000000000'
    service.createData(req.body, req.headers.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getData(req, res, next) {
    service.getData(req.query)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function getDataById(req, res, next) {
    service.getDataById(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function updateData(req, res, next) {
    service.updateData(req.body, req.user.user_id)
        .then(result => res.json(result))
        .catch(err => next(err));
}

function deleteData(req, res, next) {
    service.deleteData(req.params.id)
        .then(result => res.json(result))
        .catch(err => next(err));
}