﻿const express = require("express");
const router = express.Router();
const service = require("./device.service");
const Device = require("../../models").Device;
const { Op } = require("sequelize");

// routes
router.get("/info/:id", getInfoData);
router.get("/summery/:id", getSummeryData);
router.get("/ios/:id", getInputsOutputs);

//logger
router.get("/loggerconnecteddevice/:id", getLoggerConnectedDevice);

function getInfoData(req, res, next) {
  service
    .getInfoData(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getSummeryData(req, res, next) {
  service
    .getSummeryData(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getInputsOutputs(req, res, next) {
  service
    .getInputsOutputs(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getLoggerConnectedDevice(req, res, next) {
  service
    .getLoggerConnectedDevice(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

module.exports = router;
