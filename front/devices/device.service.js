﻿const { Op } = require("sequelize");
const mDeviceInstance = require("../../models").mDeviceInstance;
const tDeviceTelemetry = require("../../models").tDeviceTelemetry;
const tDeviceCommands = require("../../models").tDeviceCommands;
const mDeviceStatus = require("../../models").mDeviceStatus;
const mDevice = require("../../models").mDevice;
const mDeviceType = require("../../models").mDeviceType;
const mManufacturer = require("../../models").mManufacturer;
const mDeviceRelationship = require("../../models").mDeviceRelationship;
const rDeviceRelationship = require("../../models").rDeviceRelationship;

const getInfoData = async (id) => {
  const result = await mDeviceInstance.findByPk(id, { raw: true });

  const systemDeviceId = result.systemDeviceId;
  if (systemDeviceId) {
    const device = await mDevice.findByPk(systemDeviceId);

    const systemManufacturerId = device.systemManufacturerId;
    if (systemManufacturerId) {
      const manufacturer = await mManufacturer.findByPk(systemManufacturerId);
      if (manufacturer) {
        result.manufacturer = manufacturer.manufacturerName;
      }
    }
  }

  const deviceRelationship = await mDeviceRelationship.findOne({
    where: {
      relationshipName: "Logs data to",
      status: "active",
    },
    raw: true,
  });

  if (deviceRelationship) {
    const rightDeviceRelationship = await rDeviceRelationship.findOne({
      where: {
        relationshipId: deviceRelationship.relationshipId,
        leftSystemDeviceInstanceId: result.systemDeviceInstanceId,
      },
      raw: true,
    });

    if (rightDeviceRelationship) {
      const rightSystemDeviceInstance = await mDeviceInstance.findByPk(
        rightDeviceRelationship.rightSystemDeviceInstanceId,
        { raw: true }
      );

      if (rightSystemDeviceInstance) {
        result.parentDevice = rightSystemDeviceInstance.serialNo;
      }
    }
  }

  return result;
};

const getSummeryData = async (id) => {
  const deviceInstance = await mDeviceInstance.findByPk(id);
  if (deviceInstance) {
    const givenDeviceInstanceId = deviceInstance.givenDeviceInstanceId;

    const deviceTelemetry = await tDeviceTelemetry.findOne({
      where: {
        givenDeviceInstanceId,
      },
      order: [["clientTxnCaptureTime", "desc"]],
      raw: true,
    });

    if (deviceTelemetry) {
      const measurementJSONSet = deviceTelemetry.measurementJSONSet;

      if (measurementJSONSet) {
        jsonTd = measurementJSONSet.TD;

        console.log(jsonTd, "jsonTd");

        let objOpw = jsonTd.find((o) => o.n === `${givenDeviceInstanceId}:OPw`);
        const current_power = objOpw ? `${objOpw.v} ${objOpw.u}` : null;

        let objDGU = jsonTd.find((o) => o.n === `${givenDeviceInstanceId}:DGU`);
        const today = objDGU ? `${objDGU.v} ${objDGU.u}` : null;

        let objCGU = jsonTd.find((o) => o.n === `${givenDeviceInstanceId}:CGU`);
        const so_far = objCGU ? `${objCGU.v} ${objCGU.u}` : null;

        let objicT = jsonTd.find((o) => o.n === `${givenDeviceInstanceId}:icT`);
        const current_ict = objicT ? `${objicT.v} ${objicT.u}` : null;

        let objbcT = jsonTd.find((o) => o.n === `${givenDeviceInstanceId}:bcT`);
        const current_bct = objbcT ? `${objbcT.v} ${objbcT.u}` : null;

        let objCS = jsonTd.find((o) => o.n === `${givenDeviceInstanceId}:CS`);
        const statusInt = objCS ? `${objCS.v}` : null;

        let device_stastus = null;
        if (statusInt != null) {
          const systemDeviceId = deviceInstance.systemDeviceId;

          const deviceStatus = await mDeviceStatus.findOne({
            where: {
              systemDeviceId,
              givenDeviceStatusId: statusInt,
            },
            raw: true,
          });

          if (deviceStatus) {
            device_stastus = deviceStatus.deviceStatusDescription;
          }
        }

        // for tDeviceCommands

        // get firstDay and lastDay for current month
        let date = new Date(),
          d = date.getDate(),
          y = date.getFullYear(),
          m = date.getMonth();
        let firstDay = new Date(y, m, 1);
        let lastDay = new Date(y, m + 1, 0);

        let firstDayEpoc = new Date(y, m, firstDay.getDate()).getTime() / 1000;
        let lastDayEpoc = new Date(y, m, lastDay.getDate()).getTime() / 1000;

        const deviceCommands = await tDeviceCommands.findOne({
          where: {
            givenDeviceInstanceId,
            commandId: "CurrentMonthUnitGenerated",
            clientTxnCaptureTime: {
              [Op.between]: [firstDayEpoc, lastDayEpoc],
            },
          },
          order: [["clientTxnCaptureTime", "desc"]],
          raw: true,
        });

        let this_month = 0;

        if (deviceCommands) {
          const currentMonthUnitGenerated = deviceCommands.measurementJSONSet;

          if (currentMonthUnitGenerated) {
            jsonTd = currentMonthUnitGenerated.TD;

            for (let day = 1; day < d; day++) {
              let curDayEne = jsonTd.find(
                (o) => o.n === `${givenDeviceInstanceId}:D${day}`
              );
              const current_power = curDayEne ? `${curDayEne.v}` : 0;

              this_month += parseFloat(current_power);
            }

            this_month = this_month + " kWh";
          }
        }

        let firstDayOfYearEpoc = new Date(y, 0, 1).getTime() / 1000;
        let lastDayYearEpoc = new Date(y, 11, 31).getTime() / 1000;

        const deviceYearCommands = await tDeviceCommands.findOne({
          where: {
            givenDeviceInstanceId,
            commandId: "CurrentYearUnitGenerated",
            clientTxnCaptureTime: {
              [Op.between]: [firstDayOfYearEpoc, lastDayYearEpoc],
            },
          },
          order: [["clientTxnCaptureTime", "desc"]],
          raw: true,
        });

        let this_year = 0;

        if (deviceYearCommands) {
          const currentYearUnitGenerated =
            deviceYearCommands.measurementJSONSet;

          if (currentYearUnitGenerated) {
            jsonTd = currentYearUnitGenerated.TD;

            for (let month = 1; month < m; month++) {
              let curMonEne = jsonTd.find(
                (o) => o.n === `${givenDeviceInstanceId}:M${month}`
              );
              const current_power = curMonEne ? `${curMonEne.v}` : 0;

              this_year += parseFloat(current_power);
            }

            this_year = this_year + " kWh";
          }
        }

        return {
          current_power,
          today,
          so_far,
          this_month,
          this_year,
          device_stastus,
          current_ict,
          current_bct,
        };
      }
    }
  }
  return null;
};

const getInputsOutputs = async (id) => {
  const response = {};
  const systemDeviceId = id;
  if (systemDeviceId) {
    const deviceRelationship = await mDeviceRelationship.findOne({
      where: {
        relationshipName: "Provides input to",
        status: "active",
      },
      raw: true,
    });

    if (deviceRelationship) {
      const deviceRelationshipDI = await rDeviceRelationship.findOne({
        where: {
          relationshipId: deviceRelationship.relationshipId,
          rightSystemDeviceInstanceId: systemDeviceId,
        },
        raw: true,
      });

      if (deviceRelationshipDI) {
        const leftSystemDeviceInstanceId =
          deviceRelationshipDI.leftSystemDeviceInstanceId;

        const inputDevice = await mDeviceInstance.findByPk(
          leftSystemDeviceInstanceId,
          { raw: true }
        );

        if (inputDevice) {
          const leftDeviceTelemetry = await tDeviceTelemetry.findOne({
            where: {
              givenDeviceInstanceId: inputDevice.givenDeviceInstanceId,
            },
            order: [["clientTxnCaptureTime", "desc"]],
            raw: true,
          });

          let leftData = {};

          if (leftDeviceTelemetry) {
            let leftJsonTd = leftDeviceTelemetry.measurementJSONSet.TD;

            let objSPV = leftJsonTd.find(
              (o) => o.n === `${inputDevice.givenDeviceInstanceId}:SPV`
            );
            let solarPanelVoltage = objSPV ? `${objSPV.v} ${objSPV.u}` : null;

            if (solarPanelVoltage) {
              leftData.spv = solarPanelVoltage;
            }

            let objSPC = leftJsonTd.find(
              (o) => o.n === `${inputDevice.givenDeviceInstanceId}:SPC`
            );
            let solarPanelCurrent = objSPC ? `${objSPC.v} ${objSPC.u}` : null;

            if (solarPanelCurrent) {
              leftData.spc = solarPanelCurrent;
            }

            if (objSPV && objSPC) {
              leftData.pow = `${objSPV.v * objSPC.v} kW`;
            }
          }

          inputDevice.leftData = leftData;
        }

        response.inputDevice = inputDevice;

        const outputDevice = await mDeviceInstance.findByPk(systemDeviceId, {
          raw: true,
        });

        if (outputDevice) {
          const rightDeviceTelemetry = await tDeviceTelemetry.findOne({
            where: {
              givenDeviceInstanceId: outputDevice.givenDeviceInstanceId,
            },
            order: [["clientTxnCaptureTime", "desc"]],
            raw: true,
          });

          let rightData = {};

          if (rightDeviceTelemetry) {
            let rightJsonTd = rightDeviceTelemetry.measurementJSONSet.TD;

            let objUpv = rightJsonTd.find(
              (o) => o.n === `${outputDevice.givenDeviceInstanceId}:Upv`
            );
            let outputVoltage = objUpv ? `${objUpv.v} ${objUpv.u}` : null;

            if (outputVoltage) {
              rightData.upv = outputVoltage;
            }

            let objUpi = rightJsonTd.find(
              (o) => o.n === `${outputDevice.givenDeviceInstanceId}:Upi`
            );
            let outputCurrent = objUpi ? `${objUpi.v} ${objUpi.u}` : null;

            if (outputCurrent) {
              rightData.upi = outputCurrent;
            }

            let objVpv = rightJsonTd.find(
              (o) => o.n === `${outputDevice.givenDeviceInstanceId}:Vpv`
            );
            let outputVpv = objVpv ? `${objVpv.v} ${objVpv.u}` : null;

            if (outputVpv) {
              rightData.vpv = outputVpv;
            }

            let objVpi = rightJsonTd.find(
              (o) => o.n === `${outputDevice.givenDeviceInstanceId}:Vpi`
            );
            let outputVpi = objVpi ? `${objVpi.v} ${objVpi.u}` : null;

            if (outputVpi) {
              rightData.vpi = outputVpi;
            }

            let objWpv = rightJsonTd.find(
              (o) => o.n === `${outputDevice.givenDeviceInstanceId}:Wpv`
            );
            let outputWpv = objWpv ? `${objWpv.v} ${objWpv.u}` : null;

            if (outputWpv) {
              rightData.wpv = outputWpv;
            }

            let objWpi = rightJsonTd.find(
              (o) => o.n === `${outputDevice.givenDeviceInstanceId}:Wpi`
            );
            let outputWpi = objWpi ? `${objWpi.v} ${objWpi.u}` : null;

            if (outputWpi) {
              rightData.wpi = outputWpi;
            }
          }

          outputDevice.rightData = rightData;
        }

        response.outputDevice = outputDevice;
      }
    }
  }

  return response;
};

// logger

const getLoggerConnectedDevice = async (id) => {
  const result = {};

  const systemDeviceInstanceId = id;

  const deviceRelationship = await mDeviceRelationship.findOne({
    where: {
      relationshipName: "Logs data to",
      status: "active",
    },
    raw: true,
  });

  if (deviceRelationship) {
    const rightDeviceRelationship = await rDeviceRelationship.findOne({
      where: {
        relationshipId: deviceRelationship.relationshipId,
        rightSystemDeviceInstanceId: systemDeviceInstanceId,
      },
      raw: true,
    });

    if (rightDeviceRelationship) {
      const rightSystemDeviceInstance = await mDeviceInstance.findByPk(
        rightDeviceRelationship.leftSystemDeviceInstanceId,
        { raw: true }
      );

      if (rightSystemDeviceInstance) {
        result.connectedDevice = rightSystemDeviceInstance.serialNo;
        const device = await mDevice.findByPk(
          rightSystemDeviceInstance.systemDeviceId,
          { raw: true }
        );

        const systemDeviceTypeId = device.systemDeviceTypeId;

        const deviceType = await mDeviceType.findByPk(systemDeviceTypeId, {
          raw: true,
        });

        result.deviceTypeName = deviceType.deviceTypeName;
      }
    }
  }

  return result;
};

module.exports = {
  getInfoData,
  getSummeryData,
  getInputsOutputs,
  getLoggerConnectedDevice,
};
