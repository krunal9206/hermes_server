﻿const express = require("express");
const router = express.Router();
const service = require("./assettype.service");
const { check, validationResult } = require("express-validator");
const { Op } = require("sequelize");

// routes
router.post("/", createData);
router.get("/", getData);
router.get("/:id", getDataById);
router.put("/:id", updateData);
router.delete("/:id", deleteData);

module.exports = router;

function createData(req, res, next) {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ message: errors.array()[0]["msg"] });
  }

  service
    .createData(req.body, req.user.user_id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getData(req, res, next) {
  service
    .getData(req.query)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function getDataById(req, res, next) {
  service
    .getDataById(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function updateData(req, res, next) {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ message: errors.array()[0]["msg"] });
  }

  service
    .updateData(req.body, req.user.user_id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}

function deleteData(req, res, next) {
  service
    .deleteData(req.params.id)
    .then((result) => res.json(result))
    .catch((err) => next(err));
}
