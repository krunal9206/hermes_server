const setFilterPaginationSortParams = (req, res, next) => {

    if (req.query.page === undefined) {
        req.query.page = 1
    }
    //req.query.page === undefined? req.query.page = 1 : req.query.page;
    req.query.pageSize === undefined? req.query.pageSize = Number.MAX_SAFE_INTEGER : req.query.pageSize;
    req.query.searchTerm === undefined? req.query.searchTerm = null : req.query.searchTerm = req.query.searchTerm;
    req.query.sortColumn === undefined? req.query.sortColumn = null : req.query.sortColumn = req.query.sortColumn;
    req.query.sortDirection === undefined? req.query.sortDirection = null : req.query.sortDirection = req.query.sortDirection;

    next();
}

module.exports = setFilterPaginationSortParams;