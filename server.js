﻿require("rootpath")();
const express = require("express");
const app = express();
const cors = require("cors");
const jwt = require("_helpers/jwt");
const errorHandler = require("_helpers/error-handler");
const setFilterPaginationSortParams = require('./_helpers/setQueryParams');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());
app.use((req, res, next) => {
  console.log(req.headers.host + decodeURI(req.url));
  next();
})
// use JWT auth to secure the api
//app.use(jwt());
app.get('*', setFilterPaginationSortParams);
// api routes
app.use("/users", require("./users/users.controller"));
app.use("/ui", require("./ui/ui.controller"));
app.use("/contexts", require("./contexts/contexts.controller"));
app.use("/roles", require("./roles/roles.controller"));
app.use("/clients", require("./clients/clients.controller"));
app.use("/assettypes", require("./assettypes/assettypes.controller"));
app.use("/assets", require("./assets/assets.controller"));
app.use(
  "/interfacecardtypes",
  require("./interfacecardtypes/interfacecardtypes.controller")
);
app.use(
  "/interfacecards",
  require("./interfacecards/interfacecards.controller")
);
app.use("/devicetypes", require("./devicetypes/devicetypes.controller"));
app.use("/devices", require("./devices/devices.controller"));
app.use("/deviceInstanceGroups", require("./deviceinstancegroups/deviceinstancegroups.controller"));
app.use("/measurements", require("./measurements/measurements.controller"));
app.use(
  "/devicetelemetries",
  require("./devicetelemetries/devicetelemetries.controller")
);
app.use(
  "/deviceMeasurements",
  require("./deviceMeasurements/deviceMeasurements.controller.js")
);
/* app.use(
  "/stakeholdertypes",
  require("./stakeholdertypes/stakeholdertypes.controller")
); */
app.use("/stakeholders", require("./stakeholders/stakeholders.controller"));
app.use(
  "/deviceInstances",
  require("./deviceinstances/deviceinstances.controller")
);
app.use(
  "/devicerelationships",
  require("./devicerelationships/devicerelationships.controller")
);
app.use(
  "/stakeholderrelationships",
  require("./stakeholderrelationships/stakeholderrelationships.controller")
);

app.use("/dashboard", require("./dashboard/dashboard.controller"));

app.use("/front/device", require("./front/devices/devices.controller"));
app.use("/currTelemetry", require("./devicecurrenttelemetry/devicecurrenttelemetry.controller"));
app.use("/deviceInstanceGroupTelemetryPayload", require("./deviceinstancegrouptelemetrypayload/deviceinstancegrouptelemetrypayload.controller"));


// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === "production" ? 80 : 4000;
const server = app.listen(port, function () {
  console.log("Server listening on port " + port);
});
