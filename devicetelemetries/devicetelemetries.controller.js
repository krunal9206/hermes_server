﻿const express = require('express');
const router = express.Router();
const service = require('./devicetelemetry.service');

// routes
router.post('/', createData);

module.exports = router;

function createData(req, res, next) {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || (req.connection.socket ? req.connection.socket.remoteAddress : null);
    // console.log(ip);

    service.createData(req.body)
        .then(result => res.json(result))
        .catch(err => next(err));
}