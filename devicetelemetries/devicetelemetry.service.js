﻿const Modal = require('../models').DeviceTelemetry;

const createData = async (data) => {
    try {
        const device_data = JSON.parse(data.device_data);

        const deviceDataArr = [];
        if (device_data.hasOwnProperty('TH') && device_data.hasOwnProperty('TD')) {
            for(let index = 0; index < device_data['TD'].length; index++) {
                const deviceData = {};

                const clientAndInterface = device_data['TH']['bn'].split(':');
                deviceData.givenClientId = clientAndInterface[0];
                deviceData.givenInterfaceCardId = clientAndInterface[1];

                deviceData.clientTxnDateTime = device_data['TH']['t'];

                const deviceAndShortMeasurementName = device_data['TD'][index]['n'].split(':');
                deviceData.givenDeviceId = deviceAndShortMeasurementName[0];
                deviceData.shortMeasurementName = deviceAndShortMeasurementName[1];

                deviceData.deviceValue = device_data['TD'][index]['v'];
                deviceData.deviceValueUnit = device_data['TD'][index]['u'];
                deviceData.deviceTime = device_data['TD'][index]['t'];

                deviceData.createdBy = device_data['TH']['bn'];
                deviceData.updatedBy = device_data['TH']['bn'];

                deviceDataArr.push(deviceData);
            }
        }

        if(deviceDataArr.length > 0) {
            // return deviceDataArr;
            // await Modal.bulkCreate(deviceDataArr);
            // return modal;
        }

        return deviceDataArr.length;
    } catch (e) {
        console.log(e);
        return false;
    }
}

module.exports = {
    createData,
};